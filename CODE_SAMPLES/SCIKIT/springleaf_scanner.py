from collections import defaultdict
import operator

filename = 'new_train.csv'
filename = 'train.csv'

separator = ','
if 'new' in filename:
    separator = '|'

i = 0
yvarcol = None
with open(filename) as fp:
    line = fp.readline()
    line = line.strip()
    numcols = len(line.split(separator))
    for colnum, item in enumerate(line.split(separator)):
        if 'target' in item:
            yvarcol = colnum
print '-' * 80
print yvarcol, colnum, item, numcols
print '-' * 80

nline = 0
NA_TEXT = "NA,"
NA_REPL = "-1,"
for line in open(filename):
    line = line.strip()
    items = line.split(separator)
    if 'new' not in filename and len(items) != numcols:
        line = "(" + line + ")"
        line = line.replace(NA_TEXT, NA_REPL)
        items = eval(line)
        items = [x for x in items]
    if len(items) != numcols:
        print "%8s|%8s|%s" % (nline, len(items), line)
    nline += 1
print '-' * 80
print


for colnum in range(numcols):
    if colnum == yvarcol:
        continue

    d = defaultdict(int)
    for line in open(filename):
        line = line.strip()
        items = line.split(separator)
        if 'new' not in filename and len(items) != numcols:
            line = "(" + line + ")"
            line = line.replace(NA_TEXT, NA_REPL)
            items = eval(line)
            items = [x for x in items]
        if 'new' not in filename and len(items) != numcols:
            continue
        item = items[colnum]
        yvar = items[yvarcol]
        key = (item, yvar)
        d[key] = d[key] + 1

    keyset = sorted(d.items(), key=operator.itemgetter(0))
    nlines = 0
    for keypair in keyset:
        key, val = keypair
        nlines += val
        print "%32s : %s" % (key, val)
    print "KEYS:", len(keyset)
    print "LINES", nlines
    print '-' * 80
