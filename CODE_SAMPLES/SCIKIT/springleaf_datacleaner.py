import re
import sys
p = re.compile('VAR_(\d{4})')

separator = ','
xvars = {}
NA = "NA"
filename, numcols = "train.csv", 1934
filename, numcols = "test.csv", 1933


def retrieve_fselected_xvars(filename='fselect.vars'):
    xvars = {}
    with open(filename) as fp:
        lines = fp.readlines()
        for line in lines:
            findings = p.findall(line)
            if len(findings):
                varname = "VAR_%s" % findings[0]
                varnum = int(findings[0])
                xvars[varname] = varnum
                print >>sys.stderr, varname, varnum, line.strip()
        xvars['target'] = numcols
    return xvars


if __name__ == "__main__":
    xvars = retrieve_fselected_xvars(filename='fselect.vars')
    true_xvars = {}
    with open(filename) as fp:
        header = fp.readline()
        header = header.strip()
        varnames = header.split(separator)
        for colnum, varname in enumerate(varnames):
            if sum([int(k in varname) for k in xvars]):
                xvars[varname] = colnum
                print >>sys.stderr, "varname mapping", \
                    varname, colnum, xvars[varname]

    print len(xvars.keys())
    fsel_colnums = set([xvars[x] for x in xvars])
    print "|".join(["VAR_%s" % x for x in sorted(xvars.keys())])
    nline = 0
    for line in open(filename):
        nline = nline + 1
        line = line.strip()
        items = line.split(separator)
        if len(items) != numcols:
            line = "(" + line + ")"
            items = eval(line)
            items = [str(x) for x in items]
        if len(items) != numcols:
            print >>sys.stderr, nline, len(items), numcols, line
            continue
        fselect_items = [x for (i, x) in enumerate(items) if i in fsel_colnums]
        fselect_items = [x.replace('"', '') for x in fselect_items]
        out = "|".join(fselect_items)
        print out
