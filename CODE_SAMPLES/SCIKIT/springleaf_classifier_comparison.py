#!/usr/bin/python
# -*- coding: utf-8 -*-


# ####################################################################
# PARAMETERS
# ####################################################################
TEST_RUN = False
TEST_RUN = True
FIRST_NROWS = 300000
if TEST_RUN:
    FIRST_NROWS = 150000

MAX_NUM_LEVELS = 8192
PCA_NCOMPONENTS = {'method': 'basic', 'n': 384}
PCA_NCOMPONENTS = {'method': 'basic', 'n': 256}

C1_RATIO = 1./6.
C0_RATIO = 1. - C1_RATIO
DO_CLASS_BALANCING = True
APPLY_FEATURE_SELECTION = False

PERCENT_OF_DATA_TO_USE = 1./10.
PERCENT_OF_SAMPLES_TO_TRAIN_WITH = 1./20.
INTENTIONAL_AGING_WAS_EVER_WANTED = False
# ####################################################################


# ####################################################################
import pandas as pd
pd.set_option('expand_frame_repr', False)
pd.set_option('display.max_columns', 10)
pd.set_option('display.max_rows', 30)
pd.set_option('precision', 3)
# ####################################################################
import math
import time
import pydot
import numpy
from random import shuffle
from sklearn.decomposition import PCA
from sklearn.decomposition import KernelPCA
from sklearn.decomposition import RandomizedPCA
from sklearn.preprocessing import StandardScaler
from sklearn.feature_selection import chi2
from sklearn.feature_selection import SelectKBest
from sklearn.cross_validation import train_test_split
from sklearn.svm import SVC
from sklearn.lda import LDA
from sklearn.qda import QDA
from sklearn.svm import LinearSVC
from sklearn.naive_bayes import GaussianNB
from sklearn.naive_bayes import MultinomialNB
from sklearn.linear_model import SGDClassifier
from sklearn.ensemble import BaggingClassifier
from sklearn.ensemble import AdaBoostClassifier
from sklearn.tree import DecisionTreeClassifier
from sklearn.neighbors import KNeighborsClassifier
from sklearn.ensemble import RandomForestClassifier
from sklearn.ensemble import GradientBoostingClassifier
from sklearn import metrics
from sklearn import grid_search
from sklearn.metrics import f1_score
from sklearn.tree import export_graphviz
from sklearn.metrics import roc_auc_score
from sklearn.externals.six import StringIO
# ####################################################################


# ####################################################################
# GLOBALS
# ####################################################################
global TIMENOW
TIMENOW = time.time()
# ####################################################################


# ####################################################################
# CLASSIFIERS
# ####################################################################
names = ["KNN",              # 0
         "SVC",              # 1
         "LinearSVC",        # 2
         "Decision Tree",    # 3
         "GaussianNB",       # 4
         "Gradient Boost",   # 5
         "MultinomialNB",    # 6
         "AdaBoostStumps",   # 7
         "LDA",              # 8
         "QDA",              # 9
         "SGD",              # 10
         "Random Forest",    # 11
         "Radial"            # 12
         ]
# ####################################################################
classifiers = [KNeighborsClassifier(),
               SVC(probability=True, kernel="poly"),
               LinearSVC(),
               DecisionTreeClassifier(),
               GaussianNB(),
               GradientBoostingClassifier(),
               MultinomialNB(),
               AdaBoostClassifier(),
               LDA(),
               QDA(),
               SGDClassifier(shuffle=True),
               RandomForestClassifier(),
               SVC(probability=False, kernel='rbf'),
               ]
# ####################################################################
APPLY_THESE = [5, 10]
# ####################################################################


# ####################################################################
logger_fp = open('comparison.log', 'w')
# ####################################################################


# ####################################################################
# DATA LOADING
# ####################################################################
datafiles = {'T': 'DATA/new_train2.csv',
             'P': 'DATA/new_test2.csv',
             'M': 'DATA/springleaf_merged_data.csv'}

NALIST = ["-", "N/A", "NA", -1, -999999, -99999, ""]

data = pd.read_csv(datafiles['M'], sep="|", na_values=NALIST, nrows=FIRST_NROWS)

M = data.shape[0]
# ####################################################################


# ####################################################################
# STRATIFICATION/CLASS BALANCING
# ####################################################################
P = int(M * PERCENT_OF_DATA_TO_USE)
if DO_CLASS_BALANCING:
    c1 = data[data['target'] == 1]
    c1 = c1.iloc[(numpy.random.randint(1, len(c1), P * C1_RATIO))]
    C1 = c1.shape[0]

    c0 = data[data['target'] == 0]
    c0 = c0.sample(min(int(P * C0_RATIO), len(c0)))
    C0 = c0.shape[0]

    ct = data[(data['target'] != 0) & (data['target'] != 1)]
    data = pd.concat([c1, c0, ct])
    del c1, c0, ct
else:
    data = data.sample(int(M * PERCENT_OF_DATA_TO_USE))
# ####################################################################
M, N = data.shape
print >>logger_fp, '-' * 80
print >>logger_fp, M, N
# ####################################################################


# ####################################################################
# EXTRA FEATURES
# ####################################################################
cols = data.columns
col_step = 8
col_start, col_end = col_step+3, data.shape[1]-3
for i in range(col_start, col_end, col_step):
    colset = cols[i-col_step:i]
    if 'target' in colset: continue
    try:
        featurename = "fsum_%s" % i
        data[featurename] = data[colset].sum(axis=1)
        print >>logger_fp, data[featurename].describe()

        featurename = "fsum_t_%s" % i
        threshold = data[colset].mean(axis=1) - data[colset].std(axis=1)
        data[featurename] = data[colset].sum(axis=1) > threshold
        print >>logger_fp, data[featurename].describe()
    except:
        print 'problem building fsum feature', i, colset
M, N = data.shape
print >>logger_fp, '-' * 80
print >>logger_fp, M, N


# ####################################################################
# DATA CONDITIONING: FACTORS
# ####################################################################
print >>logger_fp, '-' * 80
i = 0
NA_RECODE = -1
xvars = [col for col in data.columns
         if col not in ['ID', 'target']][2:N-1]
new_xvars = []
for col in xvars:
    i += 1
    vals = sorted([x if pd.notnull(x) else NA_RECODE for x in data[col]])
    levels = set(vals + [NA_RECODE, ])
    L = len(levels)
    print >>logger_fp, levels
    print >>logger_fp, "%04d |%8s |%5s levels" % (i, col, L)
    if L < MAX_NUM_LEVELS:
        data[col] = data[col].astype('category', categories=levels,
                                     ordered=True)
        data[col] = data[col].fillna(NA_RECODE)
        new_xvars.append(col)
    else:
        # AMONG OTHERS, THESE GET DROPPED:
        # 'VAR_0404','VAR_1890','VAR_1716','VAR_0950', 'VAR_0670', 'VAR_1200',
        # 'VAR_1220','VAR_0628','VAR_0310','VAR_0330', 'VAR_0364', 'VAR_0434'
        print >>logger_fp, col, "SKIPPED"
xvars = new_xvars


# ####################################################################
# DATA CONDITIONING: NUMERICAL
# ####################################################################
i = 0
numerical_data = pd.DataFrame()
for colname in xvars:
    i += 1
    print >>logger_fp, "%04d |%8s" % (i, colname)
    vals = pd.Categorical.from_array(data[colname])
    numerical_data[colname] = [float(x) for x in vals.codes]
    print >>logger_fp, vals.codes[0:256], "..."
numerical_data['target'] = pd.Categorical.from_array(data['target'],
                                                     categories=[-1, 0, 1])
numerical_data['target'] = numerical_data['target'].fillna(NA_RECODE)
print >>logger_fp, '-' * 80
del data


# ####################################################################
# FEATURE SELECTION
# ####################################################################
if APPLY_FEATURE_SELECTION:
    # do random subsets fselects with try's
    try:
        fselector = SelectKBest(chi2, k=min(PCA_NCOMPONENTS['n']*2,
                                            numerical_data.shape[1]))
        new_data = fselector.fit_transform(numerical_data[xvars],
                                           numerical_data['target'])
        for col in xvars:
            numerical_data[col] = new_data[col]
        del new_data
    except:
        print 'problem with feature selection'


# #########################################################################
# DATASET READY
# #########################################################################
# from sklearn.datasets import make_moons, make_circles
# ("make_moons", make_moons(noise=0.3, random_state=0)),
# ("make_circs", make_circles(noise=0.2, factor=0.5, random_state=1)),
evaluation_dataset = [numerical_data[xvars], numerical_data['target']]
datasets = [("springleaf", evaluation_dataset), ]
# #########################################################################


# #########################################################################
def banner(msg, numlines=10):
    ''' displays a standard banner messsage to console output '''
    global TIMENOW
    print '-' * 80
    print "\n" * numlines
    print '-' * 80
    print msg, "%8.2fs" % (time.time() - TIMENOW)
    print '-' * 80
    print
    TIMENOW = time.time()
    return msg
# #########################################################################


# #########################################################################
def find_threshold(y_train, ypp, debug=False):
    val_max, threshold = 0.0, 0.5
    print "    THRESHOLDS :",
    for t in range(10, 90, 5):
        t = float(t)/100.
        ypc = ypp > t
        cmat = metrics.confusion_matrix(y_train, ypc)
        cmat = pd.DataFrame(cmat)

        f1_val = f1_score(y_train, ypc)
        logloss = metrics.log_loss(y_train, ypp)
        try:
            auc_score = roc_auc_score(y_train, ypp)
        except:
            auc_score = 0.0

        metric = f1_val
        threshold_test = metric >= val_max
        if t > 0.5:
            threshold_test = metric > val_max
        if threshold_test:
            val_max = metric
            threshold = t

        if debug:
            print "    THRESHOLD  :", t
            print "    TRAIN F1   :", f1_val
            print "    TRAIN LLOSS:", logloss
            print "    TRAIN AUC  :", auc_score
            print "    TRAIN CMAT :\n", cmat
            print "    " + '-' * 76
        else:
            print "(%.3f --> %8.6f),  " % (t, metric),

    print
    return threshold
# #########################################################################


# #########################################################################
def train_cv_split_idx(idx, percentage=0.65):
    shuffle(idx)
    pivot = int(len(idx) * percentage)
    train_idx = idx[:pivot]
    cv_idx = idx[pivot:(C1+C0)]
    return (train_idx, cv_idx)
# #########################################################################


# #########################################################################
# "Nearest Neighbors", "SVC", "LinearSVC", "Radial", "Decision Tree",
# "Random Forest", "AdaBoost", "GaussianNB", "MultinomialNB", "LDA", "QDA"
# #########################################################################
def configure_classifier(name, clf, K=3):
    n = PCA_NCOMPONENTS['n']
    # -----------------------------------------------------------------------
    if 'KNN' in name:
        parameters = {'n_neighbors': (5, ),
                      'p': (1, 2),
                      'weights': ('uniform', 'distance')}
        clf = BaggingClassifier(KNeighborsClassifier(n_neighbors=5, p=1,
                                                     weights='distance'),
                                n_estimators=20,
                                max_samples=10./10.,
                                max_features=8./10.)
    # -----------------------------------------------------------------------
    elif 'SVC' == name:
        parameters = {'kernel': ('poly', ),
                      'degree': (1, 2, 3),
                      'C': (1./8., 1, 8),
                      'class_weight': ('auto', None)}
        clf = grid_search.GridSearchCV(clf, parameters, cv=K, error_score=0)
    # -----------------------------------------------------------------------
    elif "NB" in name:
        clf = BaggingClassifier(clf, n_estimators=100,
                                max_samples=7./10.,
                                max_features=7./10.)
    # -----------------------------------------------------------------------
    elif "LinearSVC" == name:
        parameters = {'penalty': ('l2', 'l1'),
                      'fit_intercept': (True, False),
                      'C': (1./.12, 1./8., 1./4., 1./2., 1),
                      'class_weight': ('auto', None)}
        clf = grid_search.GridSearchCV(clf, parameters, cv=K, error_score=0)
    # -----------------------------------------------------------------------
    elif 'Radial' in name:
        parameters = {'C': (1., 2.),
                      'gamma': (1./512, 0.0),
                      'class_weight': (None, )}
        # clf = grid_search.GridSearchCV(clf, parameters, cv=K-1, error_score=0)
        clf = BaggingClassifier(clf, n_estimators=50,
                                max_samples=3./5.,
                                max_features=10./10.)
    # -----------------------------------------------------------------------
    elif 'Decision Tree' in name:
        parameters = {'max_depth': (3, 4, 5, 6, None),
                      'criterion': ('gini', 'entropy'),
                      'class_weight': ('auto', None)}
        # clf = grid_search.GridSearchCV(clf, parameters, cv=K, error_score=0)
        clf = BaggingClassifier(clf, n_estimators=50,
                                max_samples=3./3.,
                                max_features=8./10.)
    # -----------------------------------------------------------------------
    elif 'Forest' in name:
        parameters = {'n_estimators': (196, ),
                      'max_depth': (4, 8, None),
                      'max_features': (int(math.sqrt(n)), int(n/2)),
                      'class_weight': ('auto', 'subsample', None)}
        clf = grid_search.GridSearchCV(clf, parameters, cv=K-1, error_score=0)
    # -----------------------------------------------------------------------
    elif 'AdaBoostStumps' in name:
        parameters = {'n_estimators': (64, int(n/2)),
                      'learning_rate': (1./32., 1./16., 1/4., 1./2, 1, 3./2, 2)}
        clf = AdaBoostClassifier(DecisionTreeClassifier(max_depth=3))
        clf = grid_search.GridSearchCV(clf, parameters, cv=K, error_score=0)
    # -----------------------------------------------------------------------
    elif 'LDA' in name:
        clf = BaggingClassifier(clf, n_estimators=50,
                                max_samples=6./10.,
                                max_features=8./10.)
    # -----------------------------------------------------------------------
    elif 'QDA' in name:
        clf = BaggingClassifier(clf, n_estimators=50,
                                max_samples=7./10.,
                                max_features=7./10.)
    # -----------------------------------------------------------------------
    elif 'SGD' in name:
        parameters = {'loss': ('log', 'hinge', 'squared_hinge'),
                      'class_weight': ('auto', None),
                      'fit_intercept': (True, False),
                      'penalty': ('l2', 'l1')}
        clf = SGDClassifier(shuffle=True, penalty='l1', loss='squared_hinge',
                            fit_intercept=True, class_weight=None)
        clf = BaggingClassifier(clf, n_estimators=100,
                                max_samples=9./10.,
                                max_features=9./10.)
    # -----------------------------------------------------------------------
    elif 'Gradient Boost' in name:
        parameters = {'n_estimators': (100, ),
                      'max_features': ('auto', 'log2', None, 0.5),
                      'max_depth': (3, 4),
                      'learning_rate': (1./8., 1/4., 1./3., 1./2.)}
        clf = grid_search.GridSearchCV(clf, parameters, cv=K, error_score=0)
    return clf
# #########################################################################


# #########################################################################
def apply_PCA(X, nmax=10000):
    XPCA = None
    if 'kernel' == PCA_NCOMPONENTS['method']:
        rows = [x for x in set(numpy.random.randint(1, X.shape[0], nmax))]
        XPCA = KernelPCA(n_components=PCA_NCOMPONENTS['n'], kernel='rbf')
        XPCA.fit(X[rows, :])
        X = XPCA.transform(X)
    elif 'random' == PCA_NCOMPONENTS['method']:
        XPCA = RandomizedPCA(n_components=PCA_NCOMPONENTS['n'])
        XPCA.fit(X)
        X = XPCA.transform(X)
    elif 'basic' == PCA_NCOMPONENTS['method']:
        X = PCA(n_components=PCA_NCOMPONENTS['n']).fit_transform(X)
        XPCA = PCA(n_components=PCA_NCOMPONENTS['n'])
        XPCA.fit(X)
        X = XPCA.transform(X)

    try:
        print PCA_NCOMPONENTS
        print XPCA
    except:
        print PCA_NCOMPONENTS

    return X
# #########################################################################


# #########################################################################
def clf_predict(Xt, yt, name, mode, threshold=None, debug=False):
    YPC0 = clf.predict(Xt)
    if hasattr(clf, "predict_proba"):
        YPP = clf.predict_proba(Xt)
        YPP = YPP[:, 1]
    elif hasattr(clf, "decision_function"):
        YPP = clf.decision_function(Xt)

    if "SGD" in name or "LinearSVC" in name:
        YPP = (YPP - YPP.min()) / (YPP.max() - YPP.min())

    if debug:
        print YPC0[0:31], YPC0[-31:]
        print YPP[0:5], "..."
        print '-' * 80

    if "TEST" not in mode.upper():
        banner("%s: %s" % (name, mode))
        if not threshold:
            threshold = find_threshold(yt, YPP)
        else:
            new_threshold = find_threshold(yt, YPP)
            print "threshold:", threshold, new_threshold, \
                threshold == new_threshold
            threshold = new_threshold   # WARNING: PARAMETER TEST

        YPP, YPC, threshold, perf_metrics = \
            compute_metrics_for(yt, YPP, threshold)
    else:
        YPC = YPP > threshold
        perf_metrics = None

    return YPP, YPC, threshold, perf_metrics
# #########################################################################


# #########################################################################
def compute_metrics_for(yt, YPP, threshold):
    YPC = YPP > threshold
    cmat = metrics.confusion_matrix(yt, YPC)
    cmat = pd.DataFrame(cmat)
    f1_val = f1_score(yt, YPC)
    logloss = metrics.log_loss(yt, YPP)
    auc_score = roc_auc_score(yt, YPP)
    perf_metrics = metrics.classification_report(yt, YPC)

    print "FINAL THRESHOLD:", threshold
    print "FINAL F1_SCORE :", f1_val
    print "FINAL LOGLOSS  :", logloss
    print "FINAL ROC/AUC  :", auc_score
    print "FINAL CMAT     :"
    print cmat
    print '-' * 80
    print perf_metrics
    print '-' * 80

    perf_metrics = {'metrics': perf_metrics,
                    'f1_score': f1_val,
                    'logloss': logloss,
                    'auc_score': auc_score,
                    'cmat': cmat}
    return YPP, YPC, threshold, perf_metrics
# #########################################################################


# #########################################################################
def is_generative_classifier(name):
    if 'NB' in name or 'Tree' in name or 'QDA' in name:
        return True
    return False
# #########################################################################


# #########################################################################
# iterate over datasets
# #########################################################################
dataset_number = 0
for ds in datasets:
    dataset_name = ds[0]
    print dataset_name
    if "springleaf" not in dataset_name:
        X, y = ds[1]
        X = StandardScaler().fit_transform(X)
        X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=.4)
        X_cv, y_cv = X, y
    else:
        X, y = ds[1]
        print '-' * 80
        print "X", X.columns[0:31], "..."
        print "X", X.columns[-31:]
        print '-' * 80
        print "Y", [x for x in y][:12]
        print "Y", [x for x in y][-12:]
        print '-' * 80

        X = StandardScaler().fit_transform(X)

        X = apply_PCA(X, nmax=10000)

        train_idx = [j for j, x in enumerate(y != -1) if x][:(C1+C0+1)]
        test_idx = [j for j, x in enumerate(y == -1) if x]
        trainsubset_idx, cv_idx = \
            train_cv_split_idx(train_idx,
                               percentage=PERCENT_OF_SAMPLES_TO_TRAIN_WITH)

        X_train, y_train = X[trainsubset_idx, :], y[trainsubset_idx]
        X_cv, y_cv = X[cv_idx, :], y[cv_idx]
        X_test, y_test = X[test_idx, :], y[test_idx]

        print '-' * 80
        print "X(TRAIN)", X_train.shape, X_cv.shape, X_test.shape
        print X_train[0:1, 0:10]
        print "Y(TRAIN)", len(y_train), [x for x in y_train][0:31]

        print '-' * 80
        print "X(TEST)", X_test.shape
        print X_test[0:1, 0:10]
        print "Y(TEST)", len(y_test), [x for x in y_test][0:31]
        print '-' * 80

    # #####################################################################
    # iterate over classifiers
    # #####################################################################
    YP, TEST_YP, YP_BITS, TEST_BITS, thresholds, cv_thresholds, METRICS = \
        pd.DataFrame(), pd.DataFrame(), \
        pd.DataFrame(), pd.DataFrame(), {}, {}, {}
    for cnum, (name, clf) in enumerate(zip(names, classifiers)):
        if cnum not in APPLY_THESE:
            continue

        banner("%s" % name)
        print name
        print clf
        clf = configure_classifier(name, clf)
        clf.fit(X_train, y_train)
        try:
            print '-' * 80
            print name
            print clf
            print '-' * 80
            if hasattr(clf, "best_params_"):
                print clf.best_params_
            print '-' * 80
            if hasattr(clf, "feature_importances_"):
                print clf.feature_importances_
        except:
            print 'exception with respect to grid parameters'

        if 'Tree' in name:
            try:
                dot_data = StringIO()
                export_graphviz(clf, out_file=dot_data)
                graph = pydot.graph_from_dot_data(dot_data.getvalue())
                graph.write_pdf("DT.pdf")
            except:
                print 'exception with respect to tree plot'

        print '-' * 80
        YPP, YPC, trained_threshold, perf_metrics = \
            clf_predict(X_train, y_train, name, "TRAIN", threshold=None)

        print '-' * 80
        YPP, YPC, cv_threshold, perf_metrics = \
            clf_predict(X_cv, y_cv, name, "CV", threshold=trained_threshold)
        METRICS[name] = perf_metrics

        banner("EMSEMBLE: %s %s" % ([col for col in YP.columns], name))
        thresholds[name] = trained_threshold
        cv_thresholds[name] = cv_threshold
        YP[name] = (YPP + YPP * YPC)/2.0

        YP_means = numpy.array(YP.mean(axis=1))
        if INTENTIONAL_AGING_WAS_EVER_WANTED:
            YP['emsemble'] = numpy.array(YP.mean(axis=1))

        bit_name = "bit.%s" % name
        YP_BITS[bit_name] = YPP > trained_threshold
        YP_BITS['emsemble'] = numpy.array(YP_BITS.mean(axis=1))

        print YP.describe()
        print '-' * 80
        print thresholds, numpy.mean([thresholds[x] for x in thresholds])
        print thresholds, numpy.mean([cv_thresholds[x] for x in cv_thresholds])

        ems_threshold = numpy.mean([cv_thresholds[x] for x in cv_thresholds])
        ems_threshold = numpy.mean([thresholds[x] for x in thresholds])

        banner("EMSEMBLE CV WRT YPP FOR: %s" % name)
        YPP_, YPC_, _, perf_metrics_ = \
            compute_metrics_for(y_cv,
                                YP_means,
                                ems_threshold)
        METRICS['emsemble'] = perf_metrics_

        try:
            banner("PREDICTING TEST DATA FOR: %s" % name)
            YPP, YPC, _, _ = \
                clf_predict(X_test, y_test, name, "TEST",
                            threshold=trained_threshold)

            bit_name = "bit.%s" % name
            TEST_BITS[bit_name] = YPP > trained_threshold
            TEST_BITS['emsemble'] = numpy.array(TEST_BITS.mean(axis=1))
            TEST_BITS.to_csv('ypbits.csv', sep=',', header=True)

            TEST_YP[name] = (YPP + YPP * YPC)/2.0
            TEST_YP['emsemble'] = numpy.array(TEST_YP.mean(axis=1))
            TEST_YP.to_csv('yp.csv', sep=',', header=True)
        except:
            print 'problem generating/printing prediction output'

        banner('SUMMARY')
        for attr in METRICS[METRICS.keys()[0]]:
            print attr
            for x in METRICS:
                if "metrics" not in x:
                    print "%20s\t%s" % (x, METRICS[x][attr])
                else:
                    print "%20s\n%s" % (x, METRICS[x][attr])
            print '-' * 80
        print [(x, thresholds[x]) for x in names if x in thresholds]
        print [(x, cv_thresholds[x]) for x in names if x in cv_thresholds]

    TEST_YP.to_csv('yp.csv', sep=',', header=True)

print 'DONE'
