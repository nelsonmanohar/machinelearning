import re
regex1 = '''(\s*(\(["'a-zA-Z0-9_]{1,40}, ["'a-zA-Z0-9_]{1,40}\)) : \d{1,8})'''
regex2 = 'KEYS: (\d{1,7})'
p1 = re.compile(regex1, re.DOTALL)
p2 = re.compile(regex2, re.DOTALL)


def process_column_data(lines, nmin=10, nmax=50, debug=False):
    if len(lines) >= nmax or len(lines) <= nmin:
        return lines

    print
    if debug:
        for i, line in enumerate(lines):
            print i, line

    print '-' * 80
    for line in lines[:-2]:
        if 'target' in line:
            print line
            break

    for line in lines[:-2]:
        if ':' in line and 'target' not in line:
            print line

    for line in lines[-2:]:
        if 'KEYS' in line:
            print line
    print '-' * 80

    return lines


if __name__ == "__main__":
    lines = []
    divider = '-' * 80
    ncols = 0
    start, end = False, False
    for line in open('column.analysis'):
        line = line.strip()

        if divider in line and not start:
            start, end = True, False
            lines.append(line)
            continue

        elif divider in line and start:
            start, end = True, True

        if start and not end:
            lines.append(line)
            continue

        if end:
            ncols += 1
            print 'processing data for new col', ncols, len(lines)
            process_column_data(lines)
            lines = []
            start, end = False, False
