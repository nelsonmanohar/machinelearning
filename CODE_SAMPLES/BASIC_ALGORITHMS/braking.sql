
-- =======================================================================
-- IMPORTANT SIMPLIFICATION: EVERY FIELD EXPRESSED AS INTEGERS
-- =======================================================================

DROP TABLE IF EXISTS braking;
DROP TABLE IF EXISTS this_driver_braking;
DROP TABLE IF EXISTS this_driver_braking_intervals;


-- =======================================================================
-- create a dummy brakes table:
-- three drivers
-- timestamps in the 1000000
-- brake event pulse generator 
-- =======================================================================
create table braking as
        (select cast(random()*3 as integer) as driver_id, 
                cast(random()*1000000 as integer) as tstamp, 
                round(cast(random()*1 as integer)) as brake 
            from generate_series(1,1000) 
            order by driver_id, tstamp, brake);
-- =======================================================================


-- =======================================================================
-- select the records for this particular driver
-- =======================================================================
create table this_driver_braking as
                    select driver_id, tstamp, brake, 
                           row_number() over (order by tstamp) as bid
                           from braking b 
                           where driver_id = 1;
-- =======================================================================


-- =======================================================================
-- convenience function to retrieve the brake bit for above table 
-- =======================================================================
CREATE OR REPLACE 
    FUNCTION brake_bit ( bigint ) 
    RETURNS INTEGER AS 
    '
    DECLARE 
        cbit INTEGER;
    BEGIN
        SELECT brake INTO cbit 
            FROM this_driver_braking 
            WHERE bid=$1;
        RETURN ( cbit );
    END
    '
    LANGUAGE 'plpgsql';
-- =======================================================================


-- =======================================================================
-- convenience function to retrieve the timestamp of a brake event from above table 
-- =======================================================================
CREATE OR REPLACE 
    FUNCTION get_tstamp ( bigint ) 
    RETURNS INTEGER AS 
    '
    DECLARE 
        t INTEGER;
    BEGIN
        SELECT tstamp INTO t
            FROM this_driver_braking 
            WHERE bid=$1;
        RETURN ( t );
    END
    '
    LANGUAGE 'plpgsql';
-- =======================================================================


-- =======================================================================
-- temporary table containing the brake intervals for debugging purposes
-- =======================================================================
CREATE table this_driver_braking_intervals AS 
(SELECT tstamp, brake, bid,
   CASE WHEN brake_bit(bid-1)=1 AND brake_bit(bid)=1 THEN 0 
        WHEN brake_bit(bid-1)=1 AND brake_bit(bid)=0 THEN 0
        WHEN brake_bit(bid-1)=0 AND brake_bit(bid)=0 THEN 0
        WHEN brake_bit(bid-1)=0 AND brake_bit(bid)=1 THEN get_tstamp(bid)
   END as pressing,
   CASE WHEN brake_bit(bid-1)=1 AND brake_bit(bid)=1 THEN 0
        WHEN brake_bit(bid-1)=1 AND brake_bit(bid)=0 THEN get_tstamp(bid)
        WHEN brake_bit(bid-1)=0 AND brake_bit(bid)=0 THEN 0
        WHEN brake_bit(bid-1)=0 AND brake_bit(bid)=1 THEN 0
   END as releasing
   FROM this_driver_braking
   ORDER BY bid);
-- =======================================================================


-- =======================================================================
-- convenience function to retrieve the timestamp of a brake event from above table 
-- =======================================================================
CREATE OR REPLACE 
    FUNCTION get_matching_releasing( INTEGER ) 
    RETURNS INTEGER AS 
    '
    DECLARE 
        t INTEGER;
    BEGIN
        SELECT releasing INTO t
            FROM this_driver_braking_intervals 
            WHERE releasing>$1
            ORDER BY bid
            LIMIT 1;
        RETURN ( t );
    END
    '
    LANGUAGE 'plpgsql';
-- =======================================================================


-- =======================================================================
-- the actual brake intervals expressed in start, end using semantics of
-- brake interval:
    -- START INTERVAL   at 0->1 using 1's timestamp and 
    -- END INTERVAL  under 1->0 transition using 0's timestamp
-- =======================================================================
SELECT tstamp as timestamp, 
        brake, 
        bid as drivers_braking_event_index, 
        CASE WHEN pressing > 0 THEN pressing END AS braking_interval_starts, 
        CASE WHEN pressing > 0 THEN get_matching_releasing(pressing) END as braking_interval_ends
   FROM this_driver_braking_intervals
   ORDER BY bid;
-- =======================================================================


-- =======================================================================
-- Known bug, it misses the first interval transition.
-- =======================================================================

