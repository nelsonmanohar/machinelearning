
# ##################################################################################
# an O(N intervals --> M items) producer but however, with large memory consumption
# ##################################################################################
def merge_intervals(list_of_intervals, debug=0):
    '''merges a set of intervals represented via sets of
       tuples describing integer-based end-points for each intervals,
       of the form (a,b) represented interval [a,b]'''

    validation = validate_input_intervals(list_of_intervals)
    if not validation[0]:
        if debug > 0:
            print 'invalid input intervals given', list_of_intervals
            print 'using only the valid intervals given', validation[1]
        input_intervals = validation[1]

    range_intervals = [range(interval_i[0], interval_i[1]+1) for interval_i in list_of_intervals]
    if debug > 0:
        print '-' * 40
        print "INPUT:", list_of_intervals

    listing_of_elements = []
    for range_i in range_intervals:
        listing_of_elements.extend(range_i)
    listing_of_elements = [x for x in set(listing_of_elements)]

    if debug > 1:
        print "REPR:", listing_of_elements

    # special case: ()
    if len(listing_of_elements) == 0:
        shortened_intervals = [()]
        return shortened_intervals

    # special case: (a,a)
    if len(listing_of_elements) == 1:
        start = end = listing_of_elements[0]
        shortened_intervals = [(start, end)]
        return shortened_intervals

    # general case: (a1,b1), (a1, b2), etc.
    shortened_intervals = reduce_enumeration_into_intervals(listing_of_elements, debug=debug)

    if debug > 0:
        print "OUTPUT:", shortened_intervals
        print '-' * 40

    return shortened_intervals


# ##################################################################################
def validate_input_intervals(intervals):
    valid = [x for x in intervals if x[0] <= x[1]]
    if len(valid) == len(intervals):
        return (True, valid)
    return (False, valid)
# ##################################################################################


# ##################################################################################
# an O(M log M items) reducer into <n> intervals
# ##################################################################################
def reduce_enumeration_into_intervals(listing_of_elements, debug=0):
    '''compacts a list of elements into a set of intervals'''

    listing_of_elements.sort()

    matching_intervals = []

    def new_interval_run(start, end):
        '''just a tuple constructor placeholder'''
        return (start, end)

    first_item = listing_of_elements[0]
    last_item = listing_of_elements[-1]

    start_item = first_item
    current_item = first_item
    list_length = len(listing_of_elements)

    for i, item in enumerate(listing_of_elements[1:]):

        if debug > 1:
            print '-' * 40
            print item

        if current_item+1 == item:
            current_item = item
            if i == list_length-2:
                max_interval = new_interval_run(start_item, last_item)
                matching_intervals.append(max_interval)
            continue

        # special case: last item is a single item interval
        if current_item != item and i == list_length-2:
            max_interval = new_interval_run(last_item, last_item)
            matching_intervals.append(max_interval)

        max_interval = new_interval_run(start_item, current_item)
        matching_intervals.append(max_interval)
        start_item = item
        current_item = item

        if debug > 1:
            print len(intervals), intervals
            print '-' * 40

    return matching_intervals
# ###################################################


# ###################################################
def intersects( max_span, intv_j ):
    intvs = [max_span, intv_j]
    intvs.sort()
    intv0 = intvs[0]
    intv1 = intvs[1]
    if intv1[0] <= intv0[1]:
        return True
    if intv1[1] <= intv0[1]:
        return True
    if intv1[0] == intv0[1]+1:
        return True
    return False
# ###################################################


# ###################################################
# an O(N**2) merger where N = number of intervals
# ###################################################
def interval_merger(input_intervals, debug=0):
    validation = validate_input_intervals(input_intervals)
    if not validation[0]:
        if debug > 0:
            print 'invalid input intervals given', input_intervals
            print 'using only the valid intervals given', validation[1]
        input_intervals = validation[1]

    def interval_merger_kernel(input_intervals, debug=0):
        input_intervals.sort()

        output_intervals = []
        for intv_i in input_intervals:
            max_span = intv_i
            for intv_j in input_intervals:
                if intersects(max_span, intv_j):
                    max_span = min(max_span[0], intv_j[0]), max(max_span[1], intv_j[1])
            output_intervals.append(max_span)

        myIntervals = {}
        for intv in output_intervals:
            myIntervals[intv] = intv

        return [x for x in myIntervals]
    return interval_merger_kernel(interval_merger_kernel(input_intervals))
# ###################################################


# ###################################################
def verify( ALGV1=merge_intervals, ALGV2=interval_merger, debug=True ):
    import random
    NTIMES = 1000
    NINTERVALS = 10 
    INTERVAL_STARTS_RANGE = (-100, 100)
    INTERVAL_SPANS = (-2, 50)

    validation_results = []
    for idx in range(0, NTIMES):
        low = [ random.randint(*INTERVAL_STARTS_RANGE) for x in range(0, NINTERVALS) ]
        high = [ x + random.randint(*INTERVAL_SPANS) for x in low ]
        intervals = zip( low, high )

        intervals_A = sorted(ALGV1(intervals))
        intervals_B = sorted(ALGV2(intervals))
        if intervals_A != intervals_B:
            print '-' * 40
            print False, 
            validation_results.append(False)
            if debug: 
                print '-' * 40
                print idx
                print "INPUT", intervals
                print "APPROACH_A:", intervals_A 
                print "APPROACH_B:", intervals_B
                print '-' * 40
        else:
            validation_results.append(True)
    score = [ 1 for x in validation_results if x ]
    print
    print 'Out of [%s/%s] runs OK' % ( sum(score), NTIMES )
# ###################################################


# ###################################################
if __name__ == "__main__":
    intervals = [(-1,3), (1,1), (1,1), (20,24), (23,23), (23,28), (2,4), (6,8), (4,6), (12,14), (16,18), (14,16)]
    A = merge_intervals (intervals)
    B = interval_merger(interval_merger(intervals))
    print sorted(A)
    print sorted(B)
    verify()
# ###################################################

