'''binary tree balancing'''



# #############################################################################
class NODE_CLASS (object):
    def __init__(self, item=None, right=None, left=None, parent=None, dups=0, debug=False ):
        self._item = item
        self._right = right
        self._left= left 
        self._parent = parent
        self.dups = dups
        if debug: print self.__dict__
        return

    # #########################################################################
    def set_parent(self, parent=None):
        self._parent = parent
        return self._parent

    def get_parent(self):
        return self._parent
    # #########################################################################

    # #########################################################################
    def track_dups(self, increase=1):
        self.dups = self.dups + increase
        return self.dups

    def get_dups(self):
        return self.dups
    # #########################################################################

    # #########################################################################
    def set_right(self, right):
        self._right = right
        return self._right

    def get_right(self):
        return self._right
    # #########################################################################

    # #########################################################################
    def set_left(self, left):
        self._left = left
        return self._left

    def get_left(self):
        return self._left
    # #########################################################################

    # #########################################################################
    def set_item(self, item):
        self._item = item
        return self._item

    def get_item(self):
        return self._item
    # #########################################################################

    # #########################################################################
    def print_node(self, tabs=""):
        def print_data(node):
            if node != None: 
                return "%4s" % node.item
            else:
                return " ** "

        print_tuple = ( print_data(self), print_data(self.left), print_data(self.right), print_data(self.parent), self.dups)
        print "ITEM:[%4s] LEFT=[%4s] RIGHT=[%4s] PARENT=[%4s] DUPLS=[%4s]" %  print_tuple
        return print_tuple
    # #########################################################################

    right = property(get_right, set_right)
    left  = property(get_left,  set_left)
    item  = property(get_item,  set_item)
    parent = property(get_parent,  set_parent)
# #############################################################################



# #############################################################################
def has_children( node ):
    n = 0
    if node.right != None: 
        n += 1 
    if node.left != None: 
        n += 1 
    print node, n
    return n
# #############################################################################


# #############################################################################
class BINTREE_CLASS (object):
    RIGHTSIDE = 'right'
    LEFTSIDE = 'left'
    SUCCESS = True

    def __init__(self, debug=False):
        ''' initializes the root of a binary tree'''
        self.root = NODE_CLASS()
        self.debug = debug
        self.node = None
        self.parent = None
        self.side = None
        self.fnode  = None
        self.fparent = None
        self.fside  = None
        self.rchild = None
        self.lchild = None
        self.level = None
        self.lhs = []
        self.rhs = []
        self.ptree = []
        self.queue = []
        self.dfs = []
        return

    # ########################################################################
    def print_tree(self, node=None):
        ''' placeholder printing function, and traversal function'''
        print '-' * 80
        self.ptree = self.visit(self.root)
        print self.ptree
        print '-' * 80
        print
        return self.ptree
    # #########################################################################

    # #########################################################################
    def dprint(self, msg="", *nodes):
        def print_node(node):
            if node:
                return "%4s" % node.item
            return " ** "

        def print_nodes(nodes):
            for node in nodes:
                print map(print_node, (node, node.left, node.right)),
            print
            return

        if self.debug:
            print "%s %s" % (msg, print_nodes(nodes))
        return
    # #########################################################################

    # #########################################################################
    def find_bottom_right_from(self, node=None):
        '''given tree rooted at item, find item at its most bottom right'''
        if node == None:
            node = self.root

        while node.right != None:
            node = node.right

        self.dprint("Found bottom right node:", node)
        return node
    # #########################################################################

    # #########################################################################
    def find_node_for(self, item):
        '''find node containing the given item value'''
        self.fparent = None
        self.fnode = self.root
        self.fside = None

        while self.fnode != None:
            if (self.fnode).item == item:
                if self.debug:
                    print 'Found', self.fnode.item, self.fnode.print_node()
                    self.dprint("Found matching node for: [%s]" % (item), self.fnode)
                break
            elif self.fnode and item < (self.fnode).item:
                self.fside = BINTREE_CLASS.LEFTSIDE
                self.fparent = self.fnode
                self.fnode = (self.fnode).left
            elif self.fnode and item > (self.fnode).item:
                self.fside = BINTREE_CLASS.RIGHTSIDE
                self.fparent = self.fnode
                self.fnode = (self.fnode).right
            else:
                break

        return (self.fnode, self.fside, self.fparent)
    # #########################################################################

    # #########################################################################
    def merge_delete(self, node, side, parent):
        '''merges subtree to right upon delete of subtree parent on left'''
        bottom_right = self.find_bottom_right_from(node=self.node)

        # bottommost node atop of rchild of parent 
        bottom_right.right = parent.right
        # rchild of deleted node becomes rchild of parent
        parent.right = node.right
        # lchild of deleted node (parent lchild) becomes new (parent lchild)
        parent.left = node.left

        self.dprint("Merge delete done", node, bottom_right, parent)
        # self.node = None

        return (node, side, parent)
    # #########################################################################

    # #########################################################################
    def delete(self, item):
        '''locates an item and deletes its node and updates links'''
        (self.node, self.side, self.parent) = self.find_node_for(item)

        if self.node == None:
            print 'DELETE: ITEM NOT IN TREE', item
            return (self.node, self.side, self.parent)

        if self.debug:
            print (self.node.print_node(), self.side, self.parent.print_node())

        # #####################################################################
        def update_internal_node(node, side, parent):
            (self.node).parent = self.parent
            if self.side == "left":
                self.parent.left = self.node
            elif self.side == "right":
                self.parent.right = self.node
            self.dprint("Update internal node", node, parent)
            return (self.node, self.side, self.parent)
        # #####################################################################

        # #####################################################################
        def update_root_node( node, side, parent):
            self.root = (self.node).right
            (self.node).parent = None
            self.dprint("Update root node", node, parent)
            return (self.root, self.side, self.parent)
        # #####################################################################

        # #####################################################################
        def reset_as_empty_tree( node, side, parent):
            self.root = None
            self.dprint("Reset into empty tree", node, parent)
            return (self.root, self.side, self.parent)
        # #####################################################################

        # #####################################################################
        def update_as_empty_subtree( node, side, parent ):
            if self.side == BINTREE_CLASS.LEFTSIDE:
                parent.left = None
            if self.side == BINTREE_CLASS.RIGHTSIDE:
                parent.right = None
            self.dprint("Update into empty tree", node, parent)
            return (self.node, self.side, self.parent)
        # #####################################################################

        if (self.node).dups > 0:
            (self.node).dups = (self.node).dups - 1
            return (self.node, self.side, self.parent)

        if (self.node).left == None:
            if (self.node).parent == None:
                if (self.node).right != None:
                    return update_root_node(self.node, self.side, self.parent)

                if (self.node).right == None:
                    return reset_as_empty_tree(self.node, self.side, self.parent)

            if (self.node).parent != None:
                if (self.node).right != None:
                    self.node = (self.node).right
                    return update_internal_node(self.node, self.side, self.parent)

                if (self.node).right == None:
                    return update_as_empty_subtree(self.node, self.side, self.parent)

        if (self.node).left != None:
            if (self.node).parent == None:
                if (self.node).right != None:
                    return update_root_node(self.node, self.side, self.parent)

                if (self.node).right == None:
                    return reset_as_empty_tree( self.node, self.side, self.parent)

            if (self.node).parent != None:
                if (self.node).right == None:
                    self.node = (self.node).left
                    return update_internal_node(self.node, self.side, self.parent)

                if (self.node).right != None:
                    return self.merge_delete(self.node, self.side, self.parent)

    # #########################################################################

    # #########################################################################
    def visit(self, node):
        lhs_update = []
        rhs_update = []
        if node == None:
            return (lhs_update, node, rhs_update)

        if node.left == None:
            lhs_update = []
        if node.left == None:
            rhs_update = []
        if node.left:
            lhs_update = self.visit(node.left)
        if node.right:
            rhs_update = self.visit(node.right)

        return [lhs_update, [node.print_node(), ((node).dups+1)], rhs_update]
    # #########################################################################

    # #########################################################################
    def visit_dfs(self):
        '''visitation of the binary tree via dfs'''
        self.queue = [self.root,]
        self.dfs = self.queue

        while (len(self.queue)!= 0):
            print '-' * 80
            node = self.queue[0]
            node.print_node()

            if self.debug: 
                print len(self.queue), 'items'

            self.queue = self.queue[1:]

            if node.left: 
                self.queue.append(node.left)
                self.dfs.append(node.left.item)

            if node.right: 
                self.queue.append(node.right)
                self.dfs.append(node.right.item)

        return self.dfs
    # #########################################################################

    # #########################################################################
    def insert(self, item, node=None ):
        '''seeks the placement of an item to be inserted into a binary tree'''
        self.fnode = self.root
        self.rchild = None
        self.lchild = None
        self.node = None

        # #####################################################################
        def insert_duplicate(item, fnode, fparent):
            '''inserts by accounting item as duplicate of an exisiting node'''
            fnode.dups = fnode.dups + 1
            self.dprint("Insert duplicate", fnode)
            return self.fnode
        # #####################################################################

        # #####################################################################
        def insert_sandwich(item, fnode, fparent, fside):
            '''inserts a new node between two value-sandwiching nodes'''
            if fside == BINTREE_CLASS.LEFTSIDE:
                self.node = NODE_CLASS(item=item, left=fnode, parent=fparent)
                fparent.left = self.node
                fnode.parent = self.node

            if fside == BINTREE_CLASS.RIGHTSIDE:
                self.node = NODE_CLASS(item=item, right=fnode, parent=fparent)
                fparent.right = self.node
                fnode.parent = self.node

            self.dprint("Insert sandwiched node", self.node)

            return self.node
        # #####################################################################

        # #####################################################################
        def insert_leafnode(item, fnode, fparent, fside):
            '''inserts a new node as a terminal node (leaf)'''
            self.node = NODE_CLASS(item=item, parent=fparent)
            if fside == BINTREE_CLASS.RIGHTSIDE:
                fparent.right = self.node
            if fside == BINTREE_CLASS.LEFTSIDE:
                fparent.left = self.node
            self.dprint("Insert leafnode", self.node)
            return self.node
        # #####################################################################

        if self.root == None:
            self.root = NODE_CLASS(item=item, parent=None)

        if self.root.item == None:
            self.root.item = item
            self.dprint("Insert root", self.root)
            return self.root

        while self.fnode != None:
            if self.debug:
                print self.fnode.print_node()

            if self.debug: print 'Checking if duplicate'
            if (self.fnode).item == item:
                return insert_duplicate(item, self.fnode, self.fnode)

            if self.debug: print 'Checking left side'
            if item < (self.fnode).item:
                self.lchild = (self.fnode).left
                if self.debug: print 'Checking if leafnode'
                if self.lchild == None:
                    return insert_leafnode(item, self.lchild, self.fnode, BINTREE_CLASS.LEFTSIDE)
                if self.debug: print 'Checking if duplicate'
                if (self.lchild).item == item:
                    return insert_duplicate(item, self.lchild, self.fnode)
                if self.debug: print 'Checking if sandwiched'
                if item > (self.lchild).item:
                    if (self.lchild).right == None:
                        return insert_leafnode(item, self.lchild.right, self.lchild, BINTREE_CLASS.RIGHTSIDE)
                    else:
                        return insert_sandwich(item, self.lchild, self.fnode, BINTREE_CLASS.LEFTSIDE)
                if self.debug: print 'Moving to next level'
                if (self.lchild).item > item: #iterate to next level lower
                    self.fnode = self.lchild

            if self.debug: print 'Checking right side'
            if item > (self.fnode).item:
                self.rchild = (self.fnode).right
                if self.debug: print 'Checking if leafnode'
                if self.rchild == None:
                    return insert_leafnode(item, self.rchild, self.fnode, BINTREE_CLASS.RIGHTSIDE)
                if self.debug: print 'Checking if duplicate'
                if (self.rchild).item == item:
                    return insert_duplicate(item, self.rchild, self.fnode)
                if self.debug: print 'Checking if sandwiched'
                if item < (self.rchild).item:
                    if (self.rchild).left == None:
                        return insert_leafnode(item, self.rchild.left, self.rchild, BINTREE_CLASS.LEFTSIDE)
                    else:
                        return insert_sandwich(item, self.rchild, self.fnode, BINTREE_CLASS.RIGHTSIDE)
                if self.debug: print 'Moving to next level'
                if (self.rchild).item < item: #iterate to next level lower
                    self.fnode = self.rchild

        return self.fnode
    # #########################################################################
# #############################################################################



# #############################################################################
if __name__ == "__main__":
    tree  = BINTREE_CLASS()
    tree.insert(10)
    tree.insert(12)
    tree.insert(3)
    tree.insert(2)
    tree.insert(4)
    tree.delete(5)
    tree.insert(6)
    tree.insert(5)
    tree.delete(5)
    tree.insert(5)
    tree.insert(5)
    tree.insert(7)
    tree.insert(8)
    tree.insert(20)
    tree.insert(16)
    tree.insert(13)
    tree.insert(22)
    tree.delete(5)
    tree.print_tree()
    tree.visit_dfs()

    tree  = BINTREE_CLASS()
    import random
    for i in xrange(2000):
        p = random.randint(0,500)
        tree.insert(p)
    tree.visit_dfs()

    tree  = BINTREE_CLASS()
    fp = open( 'binary_tree.py', 'r' )
    data = fp.read()
    words = data.split()
    for word in words:
        tree.insert(word)
    tree.visit_dfs()
# #############################################################################
