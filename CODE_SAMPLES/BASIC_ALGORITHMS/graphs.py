#!/usr/bin/python
'''basic graph algorithms'''

#import igraph
BIGINT = 1E199


# #####################################################################
class GRAPH_CLASS(object):

    # #################################################################
    def __init__(self, filename=""):
        self.G = {}
        self.V = {}
        self.E = {}
        self.Q = []
        self.A = {}

        self.VISITED = []
        self.EDGELIST = []
        self.ORDER = 0

        self.filename = filename
        if self.filename:
            self.load_graph_via_edgelist(debug=True)
        self.V = self.G['V']
        self.E = self.G['E']

        self.adj_matrix()

        return
    # #################################################################

    # #################################################################
    def load_graph_via_edgelist( self, debug=True ):
        ''' load an edgelist containing edge and weight, 
            with all edges in one line for a given vertex'''
        with open(self.filename, 'r') as self.fp:
            lines = self.fp.readlines()
            for line in lines:
                items = [x.strip() for x in line.split(",") if x.strip()]
                n_edges = len(items[1:])
                v_vertex = items[0]

                if v_vertex not in self.V:
                    self.V[v_vertex] = []
                else:
                    print 'Node v already defined', v_vertex

                print "V[%s]:" % v_vertex,
                for i in range(1, n_edges, 2):
                    u_vertex = items[i]
                    weight = float(items[i+1])
                    edge = (v_vertex, u_vertex)
                    if edge not in self.E:
                        self.E[edge] = weight
                        self.V[v_vertex].append(u_vertex)
                    else:
                        print 'duplicate edge ignored'
    
                    if debug:
                        print "E[%s]=%s" % (edge, self.E[edge]),
                print
        self.G = {'V':self.V, 'E':self.E}
        print '-' * 80
        print self.G['V']
        for edge in self.G['E']:
            print edge,
        print
        print '-' * 80
        return self.G
    # #################################################################

    # #################################################################
    def graph_traversal(self):
        '''dfs visiting via recursion, tarjan and hopcroft'''
        print 'GRAPH TRAVERSAL'
        self.ORDER = 0
        self.EDGESET = []

        self.VISITED = {}
        for v in G['V']:
            self.VISITED[v] = 0

        while True:
            self.Q = [v for v in self.VISITED if self.VISITED[v] == 0]
            if len(self.Q) == 0:
                break

            if sum([1 for v in self.Q if self.outdegree(v) != 0]) == 0:
                break

            self.Q = [v for v in self.Q if self.outdegree(v) != 0]

            # prefers nodes with zero indegree first
            if len([v for v in self.Q if self.indegree(v) == 0 ]):
                self.Q = [v for v in self.Q if self.indegree(v) == 0]

            v = self.Q[0]

            if len(self.adjacent(v)) != 0:
                self.dfs_visit(v)
    
        print '-' * 80
        print "ACTIVATED VERTICES", self.VISITED
        print "ACTIVATED EDGE SET"
        for i, edge in enumerate(self.EDGESET):
            print i, edge
        print '-' * 80

        self.NODES = []
        for edge in self.EDGESET:
            self.NODES.extend(edge)
            self.NODES = [x for x in set(self.NODES)]
        self.DISC = set(G['V'].keys()) - set(self.NODES)
        print 'CONNECTED VERTICES:', self.NODES
        print 'DISCONNECTED VERTICES:', self.DISC
        for v in self.DISC:
            print "vertex=%4s, out[v]=%s, in[v]=%s" % (v, self.outdegree(v), self.indegree(v))
        print '-' * 80

        return self.EDGESET
    # #####################################################################

    # #####################################################################
    def adjacent(self, v):
        u_vertices = self.G['V'][v]
        return u_vertices
    # #####################################################################

    # #####################################################################
    def outdegree(self, v, debug=False):
        return len(G['V'][v])
    # #####################################################################

    # #####################################################################
    def indegree(self, v, debug=False):
        self.cnt = 0
        for u in self.V:
            if self.A[(u, v)] != 0:
                self.cnt = self.cnt + 1
        return self.cnt
    # #####################################################################

    # #####################################################################
    def dfs_visit(self, v, debug=False):
        if self.VISITED[v] == 0:
            self.ORDER = self.ORDER + 1
            self.VISITED[v] = self.ORDER

        for u in adjacent(v, self.G):
            if debug: print v, '-->', u
            if self.VISITED[u] == 0:
                self.EDGESET.append((v, u))
                self.ORDER = self.ORDER + 1
                self.VISITED[u] = self.ORDER
                self.dfs_visit(u)

        if debug:
            print self.VISITED
            print self.EDGESET
        return
    # #####################################################################

    # #####################################################################
    def adj_matrix(self):
        self.A = {}
        for v in self.V:
            for u in self.V:
                self.A[(v,u)] = 0

        for v in self.V:
            for u in self.V:
                if (v,u) in self.E:
                    self.A[v,u] = self.E[(v,u)]

        print '-' * 80
        print "%5s" % "",
        for v in sorted(self.V.keys()):
            print "%5s" % v,
        print
        print '-' * 80

        for v in sorted(self.V.keys()):
            print "%5s" % v,
            for u in self.V:
                print "%5d" % self.A[(v,u)], 
            print
        print '-' * 80
        return self.A
    # #################################################################

# #####################################################################


# #####################################################################
def adjacent(v, G, debug=True):
    u_vertices = G['V'][v]
    return u_vertices
# #####################################################################


# #####################################################################
class PATH_CLASS(object):
    def __init__(self, cost=BIGINT, path=[]):
        self.cost = cost
        self.path = path 
        return

    def set_cost(self, cost):
        self.cost = cost
        return self.cost

    def get_cost(self):
        return self.cost

    def set_path(self, path):
        self.path = path
        return self.path

    def get_path(self):
        return self.path

    cost = property(get_cost, set_cost)
    path = property(get_path, set_path)
# #####################################################################


# #####################################################################
def print_paths(P):
    print 'PATHS:'
    if not P:
        print P
        return
    for p1 in P:
        for p2 in P[p1]:
            print p1, p2, P[p1][p2]
    print '-' * 80
    return
# #####################################################################


# #####################################################################
#def topological_sort(G, x, cycles=False, debug=False):
#    return djistra_shortest_paths(G, x, cycles=True, tsort=True, debug=False)
# #####################################################################


# #####################################################################
def cycle_detection(G):
    pass
# #####################################################################


# #####################################################################
def minimum_spanning_tree(G):
    pass
# #####################################################################


# #####################################################################
def djistra_shortest_paths(G, x, cycles=False, tsort=False, debug=False):
    print 'Applying shortest paths wrt vertex %s in graph G' % x
    Q = [x]
    V = G['V'].keys()
    E = G['E']
    N = len(V)
    VISITED = dict(zip(V, [0,] * N))

    for edge in E:
        if E[edge] < 0:
            print 'Negative weights in G not allowed in DSP', edge, E[edge]
            return None
        if E[edge] == 0:
            print 'Zero weights in G not allowed in DSP', edge, E[edge]
            return None

    P = {}
    P[x] = {}
    for v in V:
        P[x][v] = [BIGINT, [x,v]]
    P[x][x] = [0, []]

    idx = 0
    (Q, G, P, VISITED, x, idx) = DSP_VISIT(Q, G, E, P, VISITED, x, idx)

    for p1 in P:
        for p2 in P[p1]:
            if P[p1][p2][0] == BIGINT:
                P[p1][p2] = [ None, []]
    return P
# #####################################################################


# #####################################################################
def DSP_VISIT(Q, G, E, P, VISITED, x, idx, cycles=False, debug=False):
    while len(Q) > 0:
        v = Q[0]
        Q = Q[1:]

        if VISITED[v] > 0: 
            if cycles:
                print 'Cycle detected, not allowed in DSP, returning results up to cycle', v
                print VISITED
                return P
            else:
                print 'Already visited vertex detected, continuing', v
                continue

        idx = idx + 1
        VISITED[v] = idx 

        for u in adjacent(v, G):
            if VISITED[u] == 0:
                Q.append(u)

            edge = (v,u)
            weight = E[edge]
            if v not in P: 
                P[v] = {}
            if u not in P[v]: 
                P[v][u] = [E[edge], [v,u]]
            if v in P and u in P[v] and weight < P[v][u]:
                P[v][u] = [E[edge], [v,u]]

            if float(P[x][v][0]) + float(P[v][u][0]) < float(P[x][u][0]):
                previous_path = P[x][v][1]
                new_weight = P[x][v][0] + P[v][u][0]
                P[x][u] = [new_weight, (previous_path, u)]
                if debug:
                    print x, u, 'updating due to new path via', v, previous_path, new_weight, P[x][u]

            if debug:
                print_paths(P)
                print '-' * 40
    return (Q,G,P,VISITED,x,idx)
# #####################################################################


# #####################################################################
if __name__ == "__main__":
    Graph_obj = GRAPH_CLASS('graph.dat')
    G = Graph_obj.G

    P = djistra_shortest_paths(G, 'v1')
    print_paths(P)

    P = djistra_shortest_paths(G, 'v20')
    print_paths(P)

    Graph_obj.graph_traversal()

# #####################################################################
