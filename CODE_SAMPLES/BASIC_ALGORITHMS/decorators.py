print '-' * 80


# ######################################################################
def character_iterator(function):
    def string_returning_function(text):
        new_text = "".join([function(x, idx=i) for i, x in enumerate(text)])
        return new_text
    return string_returning_function
# ######################################################################


# ######################################################################
def to_uppercase(x, idx=0):
    y = ord(str(x))
    if 92 <= y <= 122:
        y = y - 32
    return chr(y)
# ######################################################################
capitalized = character_iterator(to_uppercase)
print capitalized('This is a sample text from a sample book')
print '-' * 80
# ######################################################################


# ######################################################################
def to_lowercase(x, idx=0):
    y = ord(str(x))
    if 60 <= y <= 90:
        y = y + 32
    return chr(y)
# ######################################################################
undercased = character_iterator(to_lowercase)
print undercased('This is a sample text from a sample book')
print '-' * 80
# ######################################################################


# ######################################################################
@character_iterator
def to_mixedcase(x, idx=0):
    y = ord(str(x))
    if idx % 2:
        return to_uppercase(x, idx=idx)
    else:
        return to_lowercase(x, idx=idx)
# ######################################################################
mixedcased = character_iterator(to_mixedcase)
#print mixedcased('This is a sample text from a sample book')
print to_mixedcase('This is a sample text from a sample book')
print '-' * 80
# ######################################################################


# ######################################################################
def to_rot13(x, idx=0):
    y = to_lowercase(x)
    y = ord(str(y))
    if 92 <= y <= 122:
        y = y - 13
    return chr(y)
# ######################################################################
rot13encoder = character_iterator(to_rot13)
txt = rot13encoder('This is a encoded sample text from a sample book')
print txt
print '-' * 80
# ######################################################################


# ######################################################################
def from_rot13(x, idx=0):
    y = ord(str(x))
    if 92 <= (y+13) <= 122:
        y = y + 13
    return chr(y)
# ######################################################################
rot13decoder = character_iterator(from_rot13)
print 'decoded', rot13decoder(txt)
print '-' * 80
# ######################################################################


# ######################################################################
def compose(*functions):
    def sequential_apply(args):
        rval = functions[0](args)
        for f in functions[1:]:
            rval = f(rval)
        return rval
    return sequential_apply
# ######################################################################
sequential_functional_pipeline = compose(capitalized, rot13encoder, rot13decoder, to_mixedcase )
print sequential_functional_pipeline('This is a sample text from a sample book')
print '-' * 80
# ######################################################################


# ######################################################################
# Implement Words() such that it accepts a path to any ASCII text file of size <= 1TB, 
# and returns an iterable over each individual English word contained within:
# ######################################################################
class WORDS_CLASS(object):
    def __init__(self, filepath):
        self.fp = None
        self.filepath = filepath
        try:
            self.fp = open(self.filepath, 'r')
        except:
            print 'filepath not valid'
        self.EnglishPrintable = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M',
                                 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z',
                                 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm',
                                 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', '', 'y', 'z']
        return

    def read_next(self, filename=''):
        self.tchr = self.fp.read(1)
        if self.tchr == "": raise StopIteration
        if self.tchr == None: raise StopIteration
        self.word = [self.tchr, ]
        i = 0
        while self.tchr != ' ':
            i = i + 1
            self.tchr = self.fp.read(1)
            self.word.append(self.tchr)
            if i > 30:
                break
        self.word = "".join(self.word).strip()
        if self.dict_lookup(self.word):
            return self.word
        return None

    def dict_lookup(self, word):
        if self.MyEnglighDictLookup(self.word):
            return True
        else:
            return False

    def MyEnglighDictLookup(self, word):
        self.valid = sum([1 if x in self.EnglishPrintable else 0 for x in self.word])
        if self.valid > 30: return False
        if self.valid == 0: return False
        if self.valid == len(self.word):
            return True
        else:
            return False

    def Words(self):
        return self.read_next()
# #################################################################


# #################################################################
words_obj = WORDS_CLASS('decorators.py')
# #################################################################


# #################################################################
def word_iterator(function):
    def word_extractor():
        while True:
            word = function()
            if word is not None:
                yield word
    return word_extractor
# #################################################################


# #################################################################
print '-' * 80
Words = word_iterator(words_obj.Words)
for i, word in enumerate(Words()):
    if word:
        print i, word
print '-' * 80
# #################################################################


# #################################################################
# lastword
# #################################################################


# #################################################################
def invoke_with_limit(a_function, with_limit=400):
    def wrapper(*args, **kwds):
        print 'invocation_with_limit'
        plus = 0
        if 'plus' in kwds:
            plus = kwds['plus']
            print plus
        a = a_function(args) + plus
        if a < with_limit:
            print a, 'below limit', with_limit
        else:
            print a, 'on or above limit', with_limit
        print 'done'
    return wrapper
# #################################################################


# #################################################################
def add(args, plus=2):
    print args
    return sum(args)
# #################################################################
add = invoke_with_limit(add, 10)
add(8, 2, plus=2)
# #################################################################


# #################################################################
