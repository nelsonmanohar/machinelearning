import random
import numpy as np
import operator
from collections import deque
import time
import heap


# ##############################################################################
def find_min( C, debug=False ):
    '''extracts min of the array and returns remainder of array'''
    N = len(C)

    if N == 0:
        return None, []

    if N == 1:
        return C[0], []

    Cmin = C[0]
    imin = 0
    for i in xrange(0, N):
        if C[i] < Cmin:
            imin = i
            Cmin = C[i]
    C[imin] = C[0]
    C = C[1:] #C.popleft() 

    if debug:
        print Cmin, C

    return (Cmin, C)
# ##############################################################################


# ##############################################################################
def selection_sort( A, decreasing=False, debug=False ):
    '''O(N^2) selection sort'''
    #A = deque(A)
    N = len(A)
    B = deque()
    for i in xrange(0, N):
        (Cmin, A) = find_min(A)
        B.append(Cmin)
    if decreasing: return reverse(B)
    return B
# ##############################################################################


# ##############################################################################
def get_digit(x,k,xstr=True):
    '''get the kth position digit (right to left) ordering'''
    # if not xstr: x = str(x)
    M = len(x)
    if M <= k:
        return '0'
    return x[M-k-1]
# ##############################################################################


# ##############################################################################
def reverse(A):
    N = len(A)
    for i in xrange(int(N/2)):
        A[N-i-1], A[i] = A[i], A[N-i-1]
    return A

# ##############################################################################


# ##############################################################################
def check_type(x, xtype='str'):
    if isinstance(x, type(xtype)):
        return True
    return False

# ##############################################################################


# ##############################################################################
def radix_sort(A, decreasing=False, use_approx=False, ndecimals=6):
    '''O (k digits * n elements * b positions)'''
    N = len(A)
    b = 0
    B = A

    if N == 0:
        return A
    if N == 1:
        return A

    if use_approx:
        H = dict(zip(A, [str(int(x)) for x in A]))
    elif check_type(A[0], 1.0):
        D = 10**ndecimals
        H = dict(zip(A, [str(int(x*D)) for x in A]))
    else:
        H = dict(zip(A, [str(x) for x in A]))

    C = []
    while True:
        digits = [get_digit(H[x], b) for x in B]
        if len([x for x in digits if x != '0']) == 0:
            if decreasing: return reverse(C)
            return C

        C = []
        for digit in range(0,10):
            for idx, item in enumerate(digits):
                if item == str(digit):
                    C.append(B[idx])

        B = C[:]
        b = b + 1
# ##############################################################################


# ##############################################################################
def heap_sort(A, decreasing=False):
    '''O(n log n) due to the heapify operation and n deletes'''
    HEAP = heap.PRIORITY_QUEUE_CLASS(debug=False)

    N = len(A)
    for val in A:
        HEAP.enqueue(val)

    for i in range(N):
        val = HEAP.dequeue()
        A[i] = val

    if not decreasing: return reverse(A)
    return A
# ##############################################################################


# ##############################################################################
def swap(A, i, j):
    A[i], A[j] = A[j], A[i]
    return A
# ##############################################################################


# ##############################################################################
def insertion_sort(A, decreasing=False):
    op = operator.lt
    if decreasing:
        op = operator.gt

    N = len(A)
    for i in xrange(1, N):
        if op(A[i], A[i-1]):
            A = swap(A, i, i-1)
            for j in xrange(i, 0, -1):
                if op(A[j], A[j-1]):
                    A = swap(A, j, j-1)
    return A
# ##############################################################################


# ##############################################################################
def bubble_sort(A, decreasing=False):
    op = operator.lt
    if decreasing:
        op = operator.gt

    N = len(A)
    while True:
        changes = False
        for i in xrange(1, N):
            if op(A[i], A[i-1]):
                A = swap(A, i, i-1)
                changes = True
        if not changes:
            break
    return A
# ##############################################################################


# ##############################################################################
def shell_members(N, k, gap):
    idx = range(k, N, gap)
    return idx
# ##############################################################################


# ##############################################################################
def shell_sort( A, decreasing=False, gaps=[701, 301, 132, 57, 23, 10, 4, 1]):
    N = len(A)
    if N < 1000:
        gaps = [5, 3, 1]

    for k, gap in enumerate(gaps):
        for i in range(0, N):
            A = meta_insertion_sort(A, shell_members(N,i,gap), decreasing=decreasing)
    return A
# ##############################################################################


# ##############################################################################
def meta_insertion_sort(A, idxs, decreasing=False):
    if len(idxs) == 0:
        return A
    if len(idxs) == 1:
        return A

    op = operator.lt
    if decreasing:
        op = operator.gt

    N = len(idxs)
    for i in xrange(1,N):
        a = idxs[i]
        b = idxs[i-1]
        if op(A[a], A[b]):
            A = swap(A, a, b)
            for j in range(i, 0, -1):
                a = idxs[j]
                b = idxs[j-1]
                if op(A[a], A[b]):
                    A = swap(A, a, b)
    return A
# ##############################################################################


# ##############################################################################
def verify_sort( C, B, debug=False ):
    verified = sum([x for x in map( lambda x, y : abs(x-y), C, B) if x != 1 ])
    return verified
# ##############################################################################


# ##############################################################################
def benchmark(SORTER_FUNCTION, sorter_name="", M=1000, P=4096, as_numpy=False, test_fractions=False ):
    A = []
    for i in xrange(0, M):
        if test_fractions:
            A.append(random.randint(0, P)+round(random.random(),2))
        else:
            A.append(random.randint(0, P))

    t1 = time.time()
    if as_numpy:
        A = np.array(A)
    t2 = time.time()

    B = SORTER_FUNCTION(A[:])
    t3 = time.time()

    C = sorted(A)
    t4 = time.time()

    D = verify_sort(C, B)
    t5 = time.time()

    E = SORTER_FUNCTION(A[:], decreasing=True)
    t6 = time.time()

    F = reverse(C[:])
    t7 = time.time()

    G = SORTER_FUNCTION(sorted(A))
    t8 = time.time()

    H = SORTER_FUNCTION(reverse(sorted(A)))
    t9 = time.time()


    T = {   'formatting':t2-t1, 
            'sorter_function':t3-t2, 
            'python_smith_sort':t4-t3, 
            'test_verification':t5-t4,
            'sort_decreasing':t6-t5,
            'reverse_op':t7-t6,
            'sort_sorted':t8-t7,
            'sort_reversed':t9-t8}
    print '-' * 40
    print "BENCHMARK FOR SORTER:", sorter_name
    print "VERIFIED: ", not verify_sort(C, B), not verify_sort(E, F), not verify_sort(G, C), not verify_sort(H,C)
    print '-' * 40
    print len(C), len(B), D
    print "P:", [x for x in C][0:min(20, M)]
    print "S:", [x for x in B][0:min(20, M)]
    print '-' * 40
    for t in ('formatting', 'sorter_function', 'python_smith_sort', 'test_verification', 'sort_decreasing', 'reverse_op', 'sort_sorted', 'sort_reversed'):
        print "%20s %12.8f" % (t, T[t])
    print '-' * 40
    return (T, B, A)
# ##############################################################################


# ##############################################################################
if __name__ == "__main__":
    M = 1000
    T, B, A = benchmark(selection_sort, sorter_name="selection_sort", M=M)
    T, B, A = benchmark(insertion_sort, sorter_name="insertion_sort", M=M)
    T, B, A = benchmark(bubble_sort, sorter_name="bubble_sort", M=M)
    T, B, A = benchmark(radix_sort, sorter_name="radix_sort", M=M)
    T, B, A = benchmark(heap_sort, sorter_name="heap_sort", M=M)
    T, B, A = benchmark(shell_sort, sorter_name="shell_sort", M=M)

