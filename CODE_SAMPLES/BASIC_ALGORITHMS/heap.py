'''priority queue using the heap class'''

import random
import math

# #####################################################################################
class PRIORITY_QUEUE_CLASS(object):
    '''implements a priority queue using the heap class
       with enforecement of the heap property via an
       array structure and indexing'''

    # #################################################################################
    def __init__(self, debug=True):
        self.heap = []
        self.pre = None
        self.right = None
        self.left = None
        self.node = None
        self.item = None
        self.temp = None
        self.last = None
        self.debug = debug
        return

    # #################################################################################
    def is_even(self, idx):
        '''odd or even'''
        if idx % 2:
            return False
        return True

    # #################################################################################
    def get_pre(self, idx):
        '''find out the predecessor of a node'''
        if self.is_even(idx):
            self.pre = ((idx+1)-2)/2
        else:
            self.pre = ((idx+1)-1)/2
        return self.pre

    # #################################################################################
    def get_lchild(self, idx=None, pre=None):
        '''compute index of the left child of node n'''
        if pre == None:
            pre = self.get_pre(idx)
        self.left = 2*pre+1
        return self.left

    # #################################################################################
    def get_rchild(self, idx=None, pre=None):
        '''compute index of the right child of node n'''
        if pre == None:
            pre = self.get_pre(idx)
        self.right = 2*pre+2
        return self.right

    # #################################################################################
    def swap(self, idx1, idx2):
        '''swaps values at indexes idx1 and idx2 on the heap'''
        if self.debug: print 'swap', idx1, idx2
        self.temp = self.heap[idx1]
        self.heap[idx1] = self.heap[idx2]
        self.heap[idx2] = self.temp
        return (idx2, idx1)

    # #################################################################################
    def get_heap_end_idx(self):
        '''find out the next available idx on the heap'''
        self.last = len(self.heap)
        if self.debug: print 'size of queue', self.last, (self.heap)
        return self.last

    # #################################################################################
    def heap_property_met(self, pre, left, right):
        def print_status( pre, left, right):
            if self.debug:
                try:
                    print 'HEAP: ', self.heap[pre], self.heap[left], self.heap[right], self.heap
                except:
                    print '...'
                    print self.heap

        '''determine if heap property is met for node pre wrt l/r children'''
        if pre >= self.last:
            print_status( pre, left, right)
            if self.debug: print 'property met'
            return True

        if (left >= self.last and right >= self.last):
            print_status( pre, left, right)
            if self.debug: print 'property met'
            return True

        if right >= self.last:
            if self.heap[left] <= self.heap[pre]:
                print_status( pre, left, right)
                if self.debug: print 'property met'
                return True

        if left >= self.last:
            if self.heap[right] <= self.heap[pre]:
                print_status( pre, left, right)
                if self.debug: print 'property met'
                return True

        if (left < self.last and right < self.last and pre < self.last):
            if (self.heap[left] <= self.heap[pre] and self.heap[right] <= self.heap[pre]):
                print_status( pre, left, right)
                if self.debug: print 'property met'
                return True

        if self.debug:
            print_status( pre, left, right)
            print 'property not met'
        return False

    # #################################################################################
    def append(self, item):
        #self.heap[self.last] = self.item
        self.heap.append(item)
        self.last = self.get_heap_end_idx()
        return self.last-1

    # #################################################################################
    def verify(self):
        self.pre = 0
        for i, item in enumerate(self.heap):
            if 2*i+1 < len(self.heap):
                if self.heap[i] < self.heap[2*i+1]:
                    print False, i, self.heap[i], self.heap[2*i+1], self.heap
                    return False
            if 2*i+2 < len(self.heap):
                if self.heap[i] < self.heap[2*i+2]:
                    print False, i, self.heap[i], self.heap[2*i+2], self.heap
                    return False
        for i in range(len(self.heap)-1, 0, -1):
            self.pre = self.get_pre(i)
            if self.pre < 0:
                break
            if self.heap[self.pre] < self.heap[i]:
                print False, i, self.heap[self.pre], self.heap[i], self.heap
        return True

    # #################################################################################
    def enqueue(self, item):
        '''inserts an item into the heap,
           preserving the heap property'''
        print '-' * 40

        def nq_print():
            print 'enqueue:[%s] heap_size=%s heap=%s' % (self.item,self.last,self.heap)

        def nq_status(msg):
            if self.debug:
                print "... enqueue:[%s] P=%3s L=%3s R=%3s N=%s" %\
                    (msg, self.pre, self.left, self.right, self.last)

        self.item = item
        self.last = self.get_heap_end_idx()
        if self.last == 0:
            nq_print()
            self.append(item)
            return self.item

        if self.last == 1:
            self.append(item)
            if self.item > self.heap[0]:
                self.swap(0,1)
            nq_print()
            return self.item

        self.pos = self.append(item)
        self.pre = self.pos
        while True:
            self.pre = self.get_pre(self.pre)
            self.right = self.get_rchild(pre=self.pre)
            self.left = self.get_lchild(pre=self.pre)

            if self.debug: nq_status('right')
            if self.right < self.last and self.heap[self.right] > self.heap[self.pre]:
                (self.right, self.pre) = self.swap(self.right, self.pre)
    
            if self.debug: nq_status('left ')
            if self.left < self.last and self.heap[self.left] > self.heap[self.pre]:
                (self.left, self.pre) = self.swap(self.left, self.pre)

            if self.debug: nq_status('heap ')
            self.heap_property_met(self.pre, self.left, self.right)

            if self.pre <= 0:
                break

        nq_print()
        self.verify()

        return self.item


    # #################################################################################
    def pop(self):
        '''remove last element of the heap'''
        self.heap = self.heap[0:-1]
        self.last = self.get_heap_end_idx()
        return self.last

    # #################################################################################
    def dequeue(self):
        '''removes max (top) of heap 
           preserving the heap property'''
        print '-' * 40

        def dq_print():
            print 'dequeue:[%s] heap_size=%s heap=%s' % (self.item,self.last,self.heap)

        def dq_status(msg):
            if self.debug:
                print "... dequeue:[%s] P=%3s L=%3s R=%3s N=%s" %\
                        (msg, self.pre, self.left, self.right, self.last)

        self.last = self.get_heap_end_idx()
        if self.last == 0:
            self.item = None
            dq_print()
            return self.item

        if self.last == 1:
            self.item = self.heap[0]
            dq_print()
            self.pop()
            return self.item

        self.item = self.heap[0]
        self.heap[0] = self.heap[self.last-1]
        self.pop()

        self.last = self.get_heap_end_idx()
        self.pre = 0
        while True:
            self.right = self.get_rchild(pre=self.pre)
            self.left = self.get_lchild(pre=self.pre)

            if self.debug: dq_status('right')
            if self.right < self.last:
                if self.heap[self.right] > self.heap[self.left]:
                    if self.right < self.last and self.heap[self.right] > self.heap[self.pre]:
                        (self.right, self.pre) = self.swap(self.right, self.pre)

            if self.debug: dq_status('left ')
            if self.left < self.last:
                if self.right >= self.last and self.left < self.last or self.heap[self.right] <= self.heap[self.left]:
                    if self.left < self.last and self.heap[self.left] > self.heap[self.pre]:
                        (self.left, self.pre) = self.swap(self.left, self.pre)

            if self.debug: dq_status('heap ')
            if self.heap_property_met(self.pre, self.left, self.right):
                self.pre = self.right

            if self.pre >= self.last or self.right >= self.last or self.left >= self.last:
                break

        dq_print()
        self.verify()

        return self.item

    # #################################################################################
    def heap_print(self):
        print '-' * 80
        print 'HEAP STATE:'
        print '-' * 80
        tree_capacities = [ 2**x for x in range(len(self.heap)) if 2**x <= 2*len(self.heap) ]
        j = 0
        if not tree_capacities:
            return
        goal = tree_capacities[0]
        for i, item in enumerate(self.heap):
            print "[%s]=%s, " % (i, item),
            if i+1 == goal:
                j = j+1
                goal = goal + tree_capacities[j]
                print
        print
        print '-' * 80
# #####################################################################################


# #####################################################################################
if __name__ == "__main__":
    heap = PRIORITY_QUEUE_CLASS(debug=False)

    for i in range(30):
        val = random.randint(0,255)
        heap.enqueue(val)
    heap.heap_print()

    for i in range(30):
        heap.dequeue()
    heap.heap_print()



