# ##########################################
import random
# ##########################################

# ##########################################
def alreadysorted( H ):
    M = len(H)
    Hmin = H[0]
    for i in range(0,M):
        if H[i] > Hmin: 
            return False
    return True
# ##########################################


# ##########################################
def partition(H, idx, pivot, debug=False):
    M = len(H)
    print "PARTITION, enters", H, pivot
    if M==0:
        lhs = []
        rhs = []
        if ( debug ): print "PARTITION, returns", lhs, rhs
        return lhs, rhs
    elif M==1:
        lhs = H
        rhs = []
        if ( debug ): print "PARTITION, returns", lhs, rhs
        return lhs, rhs
    elif M==2:
        lhs = [ min(H) ]
        rhs = [ max(H) ]
        if ( debug ): print "PARTITION, returns", lhs, rhs
        return lhs, rhs
        
    rhs = []
    lhs = []
    for i in range(0,M):
        if H[i]>=pivot:
            rhs.append(H[i])
        if H[i]<pivot:
            lhs.append(H[i])
    if ( debug ): print "PARTITION, returns", lhs, rhs
    return (lhs, rhs )
# ##########################################


# ##########################################
def quicksort( H, debug=False ):
    M = len(H)
    if ( debug ): print "QS enters", H
    if M==0:
        print "QS, returns", []
        return []
    elif M==1:
        print "QS, returns", H
        return H
    elif M==2:
        lhs = [ min(H) ]
        rhs = max(H)
        print "QS, returns",lhs.append(rhs) 
        return lhs
    elif alreadysorted(H):
        print "QS, returns", H
        return H
    else:
        idx = random.randint( 0, M-1 )
        pivot = H[idx]
        (lhs, rhs ) = partition( H, idx, pivot )
        if lhs:
            lhs = quicksort( lhs )
        if rhs:
            rhs = quicksort( rhs )
        if lhs:
            lhs.extend(rhs)
        else:
            lhs = rhs
        print "QS, returns", lhs
    return lhs
# ##########################################




# ##########################################
if __name__ == "__main__":
    # ######################################
    print "--------------------------"
    H = [ 2, 3, 2, 1 , 5, 7, 2, 7, 0, 9, 4 ]
    print "--------------------------"
    quicksort( H )
    print "--------------------------"
    print ( sorted(H))
    print "--------------------------"
    # ######################################



