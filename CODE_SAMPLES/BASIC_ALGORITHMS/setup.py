from distutils.core import setup
from Cython.Build import cythonize

setup(
  name = 'sorting_n2',
  ext_modules = cythonize("sorting_n2.pyx"),
)
