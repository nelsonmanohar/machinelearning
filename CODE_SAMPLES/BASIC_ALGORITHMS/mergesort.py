# ##########################################
import random
import time
# ##########################################


# ##########################################
def merge( lhs, rhs ):
    print "merge", lhs, rhs
    N = len(lhs)
    M = len(rhs)
    l = 0
    r = 0
    s = []
    while( l<N or r<M ):
        if N-l and not M-r:
            s.append( lhs[l] )
            l = l + 1
        elif not N-l and M-r:
            s.append( rhs[r] )
            r = r + 1
        elif lhs[l] <= rhs[r]:
            s.append( lhs[l] )
            l = l + 1
        elif r<M and lhs[l] > rhs[r]:
            s.append( rhs[r] )
            r = r + 1

    print s
    return s
# ##########################################


# ##########################################
def merge_sort( H ):
    N = len(H)
    if N<1:
        return None
    if N==1:
        return H
    if N==2:
        return (min(H), max(H))

    pivot = int(N/2)
    lhs = H[0:pivot]
    rhs = H[pivot:N] 
    lhs = merge_sort(lhs)
    rhs = merge_sort(rhs)
    Hs = merge( lhs, rhs )
    return Hs
# ##########################################


# ##########################################
def alreadysorted( H ):
    M = len(H)
    Hmin = H[0]
    for i in range(0,M):
        if H[i] > Hmin: 
            return False
    return True
# ##########################################


# ##########################################
if __name__ == "__main__":
    # ######################################
    print "--------------------------"
    H = [ 2, 3, 2, 1 , 5, 7, 2, 7, 0, 9, 4 ]
    merge_sort ( H ) 
    print ( H )
    print "--------------------------"
    print ( sorted(H))
    print "--------------------------"
    # ######################################


