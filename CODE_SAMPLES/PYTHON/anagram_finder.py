#!/usr/bin/env python
# -*- coding: latin-1 -*-


# ##########################################################################
__doc__ = """
          This programs accepts as inputs, two strings representing:
            1] a WORD and
            2] a FILENAME
          WORD:
            represents an (English) word for which it computes candidate
            anagrams are going to be computed
          FILENAME:
            represents a list of valid (English) words used to validate
            each candidate anagram.
          The program outputs each validated candidate anagram to stdout.
          The invocation syntax is:
              python anagram_finder.py word filename
          """
__author__ = "nelsonmanohar@yahoo.com"
__date__ = "2015-08-06 20:00:00"


# ##########################################################################
__requirements__ = '''
Directions:
Given file of about 100,000 English words: write a Python, C or C++
program that finds all the anagrams of the word "empires" that exist in
the words file (just a reminder: an anagram is the result of rearranging
the letters of a word to form a new word). Your program should ideally
take only a few seconds at most to run on modern hardware. The only
inputs to your program should be the "empires" string and the words file.
Send back your Python source files. Your solution should reflect the
quality of work you would deliver to a customer, so keep in mind things
like unit/acceptance tests, comments, PEP8, etc.  '''


# ##########################################################################
print "-" * 80
print __doc__
print "-" * 80
print
# ##########################################################################


# ##########################################################################
from itertools import permutations
import os.path
import sys


# ##########################################################################
class AnagramFinderClass(object):
    ''' This class encapsulates the algorithms to search for
        all matching anagrams contained within an indexing file.
        It also provides with a functional validation routine for
        the matching logic.
    '''

    def __init__(self, a_word, dictionary_filename, do_functional_test=False):
        self.filename = dictionary_filename
        self.word = a_word
        self.word = self.anagramfinder_get_utf8()
        self.wordchars = [letter for letter in self.word]
        self.wordchars_sorted_set = sorted(set(self.wordchars))
        if not self.anagramfinder_validate_token():
            print_msg("[ERROR] Given word [%s] does not validate" % self.word)
            sys.exit(-3)
        self.do_functional_test = do_functional_test
        return

    def anagramfinder_validate_token(self):
        '''Dummy validates that a given word string is actually English'''
        return True

    def anagramfinder_get_candidates(self):
        '''Returns for a word a list of (candidate) anagrams generated via
           exhaustive permutation of its characters'''
        wchars = [x for x in self.word]
        candidate_anagrams = set(["".join(p) for p in permutations(wchars)])
        return candidate_anagrams

    def anagramfinder_get_utf8(self):
        '''Generates an utf-8 encoded version of the token'''
        try:
            utf8_word = self.word.encode('utf-8')
        except:
            # print_msg("[ERROR] problems parsing [%s] to utf-8" % self.word)
            utf8_word = None
        return utf8_word

    def anagramfinder_clean_token(self, token=""):
        '''Normalizes the token by encoding, removing whitespace, translating
           into lower case, etc'''
        token = token.encode('utf-8')
        token = token.strip()
        token = token.lower()
        return token

    def anagramfinder_anagram_checker(self, item):
        '''Check to determine if a candidate word (see item)
           represents a valid anagram of the specified word (see self.word)
        '''
        if len(self.wordchars) != len(item):
            return False

        itemchars = {}
        for letter in item:
            if letter not in itemchars:
                itemchars[letter] = 0
            if letter in itemchars:
                itemchars[letter] = itemchars[letter] + 1

        if sorted(set(itemchars)) != self.wordchars_sorted_set:
            return False

        for letter in self.wordchars:
            if itemchars[letter] > 0:
                itemchars[letter] = itemchars[letter] - 1
            else:
                return False
        return True

    def anagramfinder_find(self):
        ''' Finds given a file containing one (or more whitespace separated)
            words per line, those ones which represent anagrams of a given
            word (see self.word)
        '''
        anagrams = []

        if self.do_functional_test:
            anagram_candidate_set = self.anagramfinder_get_candidates()
            num_candidates = len(anagram_candidate_set)
            anagram_hash = dict(zip(anagram_candidate_set, (0,)*num_candidates))

        self.current_lineno = 0
        for self.current_line in open(self.filename):
            self.current_line = self.current_line.strip()
            self.current_lineno = self.current_lineno + 1
            try:
                items = [self.anagramfinder_clean_token(token=token)
                         for token in self.current_line.split()]
                items = [item for item in items if item.strip()]

                if self.do_functional_test:
                    valids = [item for item in items if item in anagram_hash]
                    if len(valids):
                        print("REAL:", valids)

                valids = [item.decode('utf-8') for item in items
                          if self.anagramfinder_anagram_checker(item)]
                if len(valids):
                    if self.do_functional_test:
                        print("OPT1", valids)
                    else:
                        anagrams.extend(valids)
            except:
                # print_msg("[WARNING] Skipped wrt problem line: %s:%s = %s" %
                #          (self.filename, self.current_lineno,
                #           self.current_line))
                continue
        return anagrams[:]
# ##########################################################################


# ##########################################################################
def print_msg(msg):
    '''prints a console message to sys.stderr (i.e, not being part of output)'''
    sys.stderr.write(msg+"\n")
    return
# ##########################################################################


# ##########################################################################
def do_argument_parsing(args):
    ''' Rudimentary argument parsing to match requirements specification
    '''
    proceed = False
    try:
        word = args[1]
        filename = args[2]
        print_msg("[STATUS] Arguments given: WORD=[%s] FILENAME=[%s]" %
                  (word, filename))
    except:
        print_msg("[ERROR] Two arguments: WORD FILENAME are required")
        sys.exit(-1)

    try:
        proceed = False
        if os.path.isfile(filename):
            with open(filename) as fp:
                fp.close()
                proceed = True
    except:
        print_msg("[ERROR] File does not exist: %s" % filename)
        sys.exit(-2)
    return word, filename, proceed
# ##########################################################################


# ##########################################################################
def apply_unit_test(test_name=""):
    ''' unit_tests:
        1: tests the handling of empty string
        2: tests the handling of a known anagram mapping
        3: tests the handling of an unknown and large word with no mappings
        '''
    if test_name in "find_anagram_of_empty_test":
        a_finder = AnagramFinderClass("", sys.argv[0])
        findings = a_finder.anagramfinder_find()
        assert len(findings) == 0
    if test_name in "find_no_anagram_test":
        a_finder = AnagramFinderClass(sys.version, sys.argv[0])
        findings = a_finder.anagramfinder_find()
        assert len(findings) == 0
    if test_name in "test_known_anagram_test":
        # torcagosarthsohcsisi241
        # tocraoasrhtshocssii241g
        a_finder = AnagramFinderClass("torcagosarthsohcsisi241", sys.argv[0])
        findings = a_finder.anagramfinder_find()
        assert "tocraoasrhtshocssii241g" in findings
        assert len(findings) == 2
    print_msg("[OK:UNIT_TEST]:" + test_name)
    return True
# ##########################################################################


# ##########################################################################
def do_unit_tests():
    '''perform some basic unit tests on the module main entry point'''
    if do_tests:
        try:
            apply_unit_test(test_name="find_anagram_of_empty_test")
            apply_unit_test(test_name="find_no_anagram_test")
            apply_unit_test(test_name="find_known_anagram_test")
        except:
            print_msg("[ERROR] Failed basic unit-tests testing")
# ##########################################################################


# ##########################################################################
if __name__ == "__main__":
    do_tests = True
    if do_tests:
        do_unit_tests()

    given_word, given_filename, proceed = do_argument_parsing(sys.argv)
    print "EN DICTIONARY=%10s " % given_filename
    print "SEARCH STRING=%10s " % given_word
    print "ANAGRAMS FOUND:"
    if proceed:
        try:
            AnagramFinder = AnagramFinderClass(given_word, given_filename,
                                               do_functional_test=False)
            found_anagrams = AnagramFinder.anagramfinder_find()
            for anagram in found_anagrams:
                print(anagram)
        except:
            print_msg("[ERROR] Unexpected problem during processing")
            print __requirements__
