# #####################################################################
def which_rows(df, rows):
    return df.index.isin(rows)
# #####################################################################


# #####################################################################
def subset(df, rows):
    return df._slice(rows)
# #####################################################################


# #####################################################################
def nrow(df):
    return df.shape[0]
# #####################################################################


