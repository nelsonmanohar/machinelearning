#!/usr/bin/env python
# -*- coding: latin-1 -*-


# #####################################################################
import nltk
import lxml
# #####################################################################


# #####################################################################
# http://stackoverflow.co/questions/8554035
# #####################################################################
from lxml.html.clean import Cleaner
# #####################################################################


# #####################################################################
# http://stackoverflow.com/questions/753052/strip-html-from-strings-in-python
# #####################################################################
from HTMLParser import HTMLParser
# #####################################################################


# #####################################################################
# http://stackoverflow.com/questions/753052/strip-html-from-strings-in-python
# #####################################################################
class MLStripper(HTMLParser):
    def __init__(self):
        self.reset()
        self.fed = []
    def handle_data(self, d):
        self.fed.append(d)
    def get_data(self):
        return ''.join(self.fed)


def strip_tags(html):
    s = MLStripper()
    s.feed(html)
    return s.get_data()
# #####################################################################


# #####################################################################
# http://stackoverflow.co/questions/8554035
# #####################################################################
def clean_javascript(html, url=None, pretty=True):
    # -----------------------------------------------------------------
    html = lxml.etree.HTML(html)
    lxml.etree.strip_elements(html, 'script')
    result = lxml.etree.tostring(html, pretty_print=pretty, method="html")
    # -----------------------------------------------------------------
    cleaner = Cleaner()
    cleaner.javascript = True
    cleaner.style = True
    result = cleaner.clean_html(lxml.html.tostring(html))
    return result
# #####################################################################


# #####################################################################
if __name__ == "__main__":
    fp = open('index.html', 'r')
    HTML = fp.read()
    HTML = clean_javascript(HTML)
    TXT = strip_tags(HTML)
    WORDS = nltk.word_tokenize(TXT)
    WORDS = [str(word) for word in WORDS]
    TEXT = nltk.Text(WORDS)
