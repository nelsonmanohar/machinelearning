#!/usr/bin/env python
# -*- coding: latin-1 -*-


# #####################################################################
import warnings
warnings.simplefilter('default')
import sys
import time
import os.path
import argparse
import subprocess
# ---------------------------------------------------------------------
try:
    import cProfile, pstats, StringIO
    from pycallgraph import PyCallGraph
    from pycallgraph.output import GraphvizOutput
except:
    pass
# ---------------------------------------------------------------------
import pandas as pd
pd.set_option('display.max_rows', 500)
pd.set_option('display.max_columns', 150)
pd.set_option('expand_frame_repr', False)
# ---------------------------------------------------------------------
import random
# ---------------------------------------------------------------------
import numpy as np
import matplotlib.pyplot as plt
# ---------------------------------------------------------------------
from sklearn.pipeline import Pipeline
# ---------------------------------------------------------------------
from sklearn.preprocessing import MultiLabelBinarizer
from sklearn.feature_extraction import DictVectorizer
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.feature_extraction.text import TfidfTransformer
from sklearn.tree import DecisionTreeClassifier
# ---------------------------------------------------------------------
from sklearn.feature_selection import RFE
from sklearn.feature_selection import SelectKBest
from sklearn.feature_selection import chi2
# ---------------------------------------------------------------------
from sklearn.multiclass import OneVsRestClassifier
from sklearn.svm import SVC
from sklearn.svm import LinearSVC
from sklearn.naive_bayes import BernoulliNB
from sklearn.naive_bayes import GaussianNB
from sklearn.naive_bayes import MultinomialNB
from nltk.classify import SklearnClassifier
# ---------------------------------------------------------------------
from sklearn import cross_validation
from sklearn import metrics
# ---------------------------------------------------------------------
from pandas_basics import *
# #####################################################################


# #####################################################################
from py_nltk_feature_generator import *
# #####################################################################


# #####################################################################
global POS_TAGGER
debug = 0
NOW = time.time()
# #####################################################################


# #####################################################################
def HEADER(what):
    global NOW
    print '-' * 80
    t = time.time()
    print "%.3f %s" % (t-NOW, what)
    NOW = t
    return NOW
# #####################################################################


# #####################################################################
def main(args, debug=False):
    T0 = HEADER('STARTED')
    FEATURE_GENERATOR = feature_generator(filename=args.filename, top_n=args.top_n, minwordlen=args.minwordlen, encoding="utf-8", do_chunking=False, debug=False)
    fvecs = FEATURE_GENERATOR.generate_vectors()
    if debug:
        for i, fv in enumerate(fvecs):
            print i, fv
    T1 = HEADER('DONE: FVEC GENERATION')
    print "%.4f %s" % ((T1-T0)/len(fvecs), "SECONDS PER VECTOR")
    return fvecs
# #####################################################################


# #####################################################################
class PART_OF_SPEECH_BOOTRAPPER(object):
    CUTS_WORDLEN = [0, 2, 4, 6, 8, 12, 16, 24, 128]
    CUTS_WORDNET = [0, 2, 4, 6, 8, 12, 16, 24, 48, 128, 256]

    # #################################################################
    def __init__(self, df=(), debug=False, classifiers=(), names=()):
        self.df = df
        self.debug = debug
        self.randomized = False
        self.y, self.x = (), ()
        self.t_idx, self.p_idx = (), ()
        self.clf_stats = []
        # -------------------------------------------------------------
        if not len(classifiers) or not len(names):
            self.classifiers = [OneVsRestClassifier(LinearSVC()),
                                DecisionTreeClassifier(criterion='gini', max_depth=18, max_features='auto', min_samples_leaf=6, min_samples_split=7),
                                MultinomialNB(),
                                BernoulliNB(),
                                OneVsRestClassifier(SVC(kernel='poly', degree=2, C=1)),
                                OneVsRestClassifier(SVC(kernel='rbf', gamma=.5, C=1))]
            self.cnames = ["LinearSVC",
                           "DecisionTree",
                           "BernoulliNB",
                           "MultinomialNB",
                           "PolySVM",
                           "RBFSVM"]
        else:
            self.classifiers = classifiers
            self.cnames = names
        return
    # #################################################################

    # #################################################################
    def get_classifiers(self, up_to=1):
        return zip(self.classifiers, self.cnames)[0:up_to]
    # #################################################################

    # #################################################################
    def apply_data_conditioning(self, df=(), skip=False):
        if len(df):
            self.df = df

        if not skip:
            self.df['m1_wordlen'] = pd.cut(self.df['m1_wordlen'], self.CUTS_WORDLEN, right=False)
            self.df['i0_wordlen'] = pd.cut(self.df['i0_wordlen'], self.CUTS_WORDLEN, right=False)
            self.df['p1_wordlen'] = pd.cut(self.df['p1_wordlen'], self.CUTS_WORDLEN, right=False)
            self.df['num_antonyms'] = pd.cut(self.df['num_antonyms'], self.CUTS_WORDNET, right=False)
            self.df['num_synonyms'] = pd.cut(self.df['num_synonyms'], self.CUTS_WORDNET, right=False)
            self.df['num_hyponyms'] = pd.cut(self.df['num_hyponyms'], self.CUTS_WORDNET, right=False)
            self.df['alternatives'] = pd.cut(self.df['alternatives'], self.CUTS_WORDNET, right=False)

        for colname in self.df.columns:
            self.df[colname] = self.df[colname].astype('category')

        HEADER('DONE: DATAFRAME GENERATION')
        return self.df
    # #################################################################

    # #################################################################
    def apply_feature_selection(self, ycol='nltk_pos', df=(), skip=True):
        if not skip:
            out = subprocess.check_output(['/usr/bin/Rscript', 'feature_selection.R'])  #, args.filename])
            lines = out.split('\n')
            idx = [(i, x.find('attr_importance')) for (i, x) in enumerate(lines) if 'attr_importance' in x]
            print '-' * 80
            for itemrow in lines:
                print itemrow
            print '-' * 80
            features = pd.read_csv('features.csv', header=False, sep=',')
            attrs = [x for x in features['x']]
            chi_squared_fselcols = [ycol, 'word'] + attrs
            self.df = self.df[chi_squared_fselcols]
            self.df.describe()
        HEADER('DONE: FEATURE SELECTION')
        return df
    # #################################################################

    # #################################################################
    def write_df_to_csv(self, filename):
        self.df.to_csv(path_or_buf=filename, mode='w', encoding=None,
                       sep="\t", header=True,
                       line_terminator='\n', escapechar=None,
                       index=True, index_label=None,
                       quoting=None, quotechar='"', doublequote=True,
                       decimal='.')
        HEADER('DONE: WRITING TO CSV')
        return
    # #################################################################

    # #################################################################
    def define_x_and_y_columnsets(self, ycol=(), dropcols=()):
        which_cols = [x for x in self.df.columns]
        self.ycol = ycol
        self.dropcols = list(dropcols)
        self.xcols = list(set(which_cols).difference([self.ycol,] + self.dropcols))
        print "%s ~ %s" % (self.ycol, self.xcols)
        print '-' * 80
        return (self.xcols, self.ycol)
    # #################################################################

    # #################################################################
    def _randomize(self, split_ratio):
        self.NROWS = nrow(self.df)
        self.split_pivot = int(self.NROWS/split_ratio)
        self.idx = range(self.NROWS)
        random.shuffle(self.idx)
        print "INDEXING BUILT", self.idx[0:10], len(self.idx)
        print '-' * 80
        self.randomized = True
        return (self.idx, self.split_pivot)
    # #################################################################

    # #################################################################
    def in_training_mode(self, mode):
        if "TRAIN" in mode.upper():
            return True
        return False
    # #################################################################

    # #################################################################
    def get_indexes_for(self, mode="TRAIN", split_ratio=20):
        if not self.randomized:
            (self.idx, self.split_pivot) = self._randomize(split_ratio)
        if self.in_training_mode(mode):
            self.t_idx = self.idx[:self.split_pivot]
            if self.debug:
                print "TRAIN INDEX BUILT", len(self.t_idx)
            return self.t_idx
        else:
            self.p_idx = self.idx[self.split_pivot:]
            if self.debug:
                print "TEST INDEX BUILT", len(self.p_idx)
            return self.p_idx
    # #################################################################

    # #################################################################
    def get_x_and_y(self):
        self.y = self.df[self.ycol]
        self.x = pd.get_dummies(self.df[self.xcols])
        self.words = [w for w in self.df['word']]
        self.ytrue = [y for y in self.df[self.ycol]]
        print "Y:\n", self.y.shape
        print '-' * 80
        print "X:\n", self.x.shape
        print '-' * 80
        print "WORDS:\n", self.words[0:10]
        print '-' * 80
        return (self.x, self.y)
    # #################################################################

    # #################################################################
    def get_dataset(self, mode="TRAIN", split_ratio=10):
        if not len(self.y) or not len(self.x):
            (self.x, self.y) = self.get_x_and_y()
        tmp_idx = self.get_indexes_for(mode=mode, split_ratio=split_ratio)
        x = self.x._slice(tmp_idx)
        y = self.y._slice(tmp_idx)
        print "DATASET Y:", mode, y.shape
        print "DATASET X:", mode, x.shape
        return (x, y)
    # #################################################################

    # #################################################################
    def fit_predict(self, clf, name=""):
        print
        print '-' * 80
        print name
        print clf
        print '-' * 80
        self.fit(clf, name=name)
        stats = self.predict(clf, name=name)
        self.clf_stats.append(stats)
        print '-' * 80
        return stats
    # #################################################################

    # #####################################################################
    def get_ytrue(self, mode="TRAIN"):
        if self.in_training_mode(mode):
            idx = self.t_idx
        else:
            idx = self.p_idx
        ytrue = [self.ytrue[x] for x in idx]
        words = [self.words[x] for x in idx]
        if self.debug:
            print "YTRUE", mode, len(ytrue), len(words)
            print '-' * 80
        return (ytrue, words)
    # #####################################################################

    # #####################################################################
    def fit(self, clf, name="", cv=True):
        print '-' * 80
        print name + ":TRAIN"
        (train_x, train_y) = self.get_dataset(mode="TRAIN")
        clf.fit(train_x, train_y)
        yp_train = clf.predict(train_x)
        print '-' * 80
        print metrics.classification_report(train_y, yp_train)
        print '-' * 80
        if cv:
            print '-' * 80
            self.cmat = metrics.confusion_matrix(train_y, yp_train)
            print self.cmat
            print '-' * 80
        (mismatches, y_true) = self.identify_mismatches(yp_train, mode="TRAIN")
        return (clf, mismatches, yp_train, y_true)
    # #####################################################################

    # #####################################################################
    def predict(self, clf, name="", cv=False):
        print '-' * 80
        print name + ":TEST"
        (predict_x, predict_y) = self.get_dataset(mode="TEST")
        yp_test = clf.predict(predict_x)
        print '-' * 80
        self.labels = set(yp_test)
        self.labels.update(predict_y)
        self.labels = [x for x in self.labels]
        print self.labels
        print '-' * 80
        self.cmat = metrics.confusion_matrix(predict_y, yp_test, labels=self.labels)
        if cv:
            print pd.DataFrame(self.cmat)
            print '-' * 80
        self.cmat = self.cmat.astype('float') / self.cmat.sum(axis=1)[:, np.newaxis]
        fig = plt.figure()
        ax = fig.add_subplot(111)
        cax = ax.matshow(self.cmat)
        plt.title('Confusion matrix of the classifier')
        plt.ylabel('True label')
        plt.xlabel('Predicted label')
        fig.colorbar(cax)
        ax.set_xticklabels([''] + self.labels)
        ax.set_yticklabels([''] + self.labels)
        plt.savefig('test_cv-%s.png' % int(time.time()))
        print metrics.classification_report(predict_y, yp_test)
        print '-' * 80
        if cv:
            print cross_validation.cross_val_score(clf, predict_x, predict_y, scoring='accuracy')
            print cross_validation.cross_val_score(clf, predict_x, predict_y, scoring='f1_micro')
            print '-' * 80
        (mismatches, y_true) = self.identify_mismatches(yp_test, mode="TEST")
        return (clf, mismatches, yp_test, y_true)
    # #####################################################################

    # #################################################################
    def identify_mismatches(self, yp, mode="TRAIN"):
        (y_true, wrt_words) = self.get_ytrue(mode=mode)
        mismatches = [tag_pairing for (i, tag_pairing) in enumerate(zip(yp, y_true, wrt_words))
                      if tag_pairing[0] != tag_pairing[1]]
        if self.debug:
            print '-' * 80
            for i, mismatch in enumerate(mismatches):
                print "[%4s:%4s (%10s)], " % tuple(mismatch),
                if i % 5 == 0:
                    print
            print
        print '-' * 80
        print "MISMATCH STATS", mode, len(mismatches), len(y_true)
        self.match_rate = 100 - round(float(len(mismatches))/float(len(y_true)), 2) *100
        print "POS MATCH RATE: %s" % self.match_rate
        return (mismatches, y_true)
    # #################################################################

    # #####################################################################
    def summarize(self, M=5000, items_per_row=5):
        print
        print '-' * 80
        for stats in self.clf_stats:
            print "CLASSIFIER", stats[0]
            print '-' * 80
            m = min(M, len(stats[1]))
            for i in range(0, m, items_per_row):
                print "MISMATCHES:", stats[1][i:i+items_per_row]
            print len(stats[1]), len(self.p_idx)
            self.mismatch_rate = round(float(len(stats[1]))/float(len(self.p_idx)) * 100., 2), "percent mismatches"
            print '-' * 80
            print "MISMATCH RATE", self.mismatch_rate
            print '-' * 80
        print
        return self.clf_stats
    # #####################################################################


# #####################################################################
def apply_pos_tagger(df):
    global POS_TAGGER
    POS_TAGGER = PART_OF_SPEECH_BOOTRAPPER(df=df)
    POS_TAGGER.write_df_to_csv(filename)
    POS_TAGGER.apply_data_conditioning(skip=False)
    POS_TAGGER.apply_feature_selection(skip=False)
    POS_TAGGER.define_x_and_y_columnsets(ycol='nltk_pos', dropcols=['Unnamed: 0', 'word'])
    for (clf, cname) in POS_TAGGER.get_classifiers(up_to=1):
        POS_TAGGER.fit_predict(clf, name=cname)
    stats = POS_TAGGER.summarize()
    HEADER('DONE')
    return stats
# #####################################################################


# #####################################################################
if __name__ == "__main__":
    parser = argparse.ArgumentParser(prog='py_nltk_pos_tagger.py', description='This program illustrates the generation of simple part-of-speech (pos) predictive features for a text.')
    # -----------------------------------------------------------------
    parser.add_argument("--filename",   default='index.html',  help='filepath to data file containing the lines to be sampled.')
    parser.add_argument("--csvfile",    default='data.csv',    help='filepath to data file to output the feature vectors to.')
    parser.add_argument("--top_n",      type=int, default=250, help='number of top tokens to use in various ranking features.')
    parser.add_argument("--minwordlen", type=int, default=2, help='minimum length of a valid verb, name, adjective.')
    parser.add_argument("--conditioning",type=int, default=1, help='apply data conditioning? values: 1/0')
    parser.add_argument("--do_profile", type=int, default=0, help='do profiling; values: 1/0.')
    parser.add_argument("--do_pygraph", type=int, default=0, help='do flow graphing; values: 1/0.')
    parser.add_argument("--do_chunking",type=int, default=0, help='do localized chunk-based training/prediction; values 1/0.')
    parser.add_argument("--reuse_tags", type=int, default=0, help='reuse previously computed tags for the given file 1/0.')
    # -----------------------------------------------------------------
    parser.add_argument("--debug", type=int, default=0, help='debug mode 1/0.')
    args = parser.parse_args()
    try:
        print >>sys.stderr, '-' * 80
        print [("%s-->%s" % (arg, args.__dict__[arg])) for arg in args.__dict__ if not arg.startswith('_')]
        print >>sys.stderr, '-' * 80
        print >>sys.stderr, "datafile    :", args.filename
        print >>sys.stderr, "output file :", args.csvfile
        print >>sys.stderr, "high_freq n :", args.top_n
        print >>sys.stderr, "minwordlen  :", args.minwordlen
        print >>sys.stderr, "do_profiling:", args.do_profile != 0
        print >>sys.stderr, "do_chunking :", args.do_chunking != 0
        print >>sys.stderr, "reuse_tags  :", args.reuse_tags != 0
        print >>sys.stderr, '-' * 80
    except:
        parser.print_help()
    # -----------------------------------------------------------------

    if not args.reuse_tags:
        try:
            golden_tags_filename = '%s.goldentags' % args.filename
            if os.path.isfile(golden_tags_filename):
                os.remove(golden_tags_filename)
                print 'removed %s' % golden_tags_filename
        except:
            pass
    else:
        print 'reusing %s' % golden_tags_filename

    # -----------------------------------------------------------------
    if args.do_profile:
        profiler = cProfile.Profile()
        profiler.enable()
        if args.do_pygraph:
            with PyCallGraph(output=GraphvizOutput()):
                fvecs = main(args)
        else:
            fvecs = main(args)
        profiler.disable()
        s = StringIO.StringIO()
        sortby = 'cumulative'
        ps = pstats.Stats(profiler, stream=s).sort_stats(sortby)
        ps.print_stats(.2)
        print s.getvalue()
        print '-' * 80
    else:
        fvecs = main(args)
    HEADER('DONE: FVEC GENERATION')
    # -----------------------------------------------------------------

    if not args.do_chunking:
        df = pd.DataFrame.from_dict(fvecs)
        filename = args.csvfile
        apply_pos_tagger(df)
    else:
        N = len(fvecs)
        start = 0
        df = []
        STEPS = 10
        for i in range(STEPS):
            print '-' * 80
            stop = int(N/STEPS) * (i+1)
            print i, start, stop, N
            frame = pd.DataFrame.from_dict(fvecs[start:stop])
            start = stop
            filename = "%s_part_%s.csv" % ( args.csvfile, i)
            # frame.to_csv(path_or_buf=filename, sep="\t", header=True,
                # index=True, index_label=None, mode='w', encoding=None, quoting=None, quotechar='"',
                # line_terminator='\n', doublequote=True, escapechar=None, decimal='.')
            apply_pos_tagger(frame)
            HEADER('DONE WITH PART [%i out of %s]' % (i+1, STEPS))
            print '\n' * 10
    # -----------------------------------------------------------------
    HEADER('DONE')

    # v = DictVectorizer()
    # X = v.fit_transform(fvecs)
    # y = df['nltk_pos']
    # Y = MultiLabelBinarizer().fit_transform(y)

    # -----------------------------------------------------------------

    # create the RFE model and select 3 attributes
    #clf = OneVsRestClassifier(LinearSVC())
    #rfe = RFE(clf, 8, step=1, verbose=1)
    #rfe = rfe.fit(x,y)
    #print(rfe.support_)
    #print(rfe.ranking_)
    #TRAINING_SET_X = rfe.transform( TRAINING_SET_X)
    #PREDICT_SET_Y = rfe.transform( PREDICT_SET_Y)

    # X_new = SelectKBest(chi2, k=2).fit_transform(x, y)
    # X_new.shape
