#!/usr/bin/python

# #################################################################
# copyright nelsonmanohar
__license__= '''This module (version Feb/2015), Copyright (C) Nelson R. Manohar,
comes with ABSOLUTELY NO WARRANTY.  This is free software, and you are welcome to 
redistribute it under conditions of the GNU General Public License.'''
__author__ = "NelsonManohar"
__date__   = "Feb2015"
__doc__    = '''minhash and lshash functions for comparing documents
(i.e., documents share elements/values from an implicitly common lexicon)
as well as dataframes (i.e., similar values across columns/featues do not
necessarily represent the same quantify, factor level, physical meaning)'''
print '-' * 80
print __doc__
print '-' * 80
print __license__
print '-' * 80
# #################################################################


# #################################################################
import math
import random
from collections import defaultdict
import numpy as np
import pandas as pd
import time
import bitarray
import argparse
import sys
import minhash
import lshashing
# #################################################################


# #################################################################
# stackoverflow.com/questions/11707586
pd.set_option('display.max_rows', 100)
pd.set_option('display.max_columns', 30)
pd.set_option('display.width', 200)
pd.set_option('display.height', 200)
pd.set_option('display.precision', 2)
# #################################################################


# #################################################################
if __name__ == "__main__":
    parser = argparse.ArgumentParser(prog='minhash.py',
             description='This program computes minhash-based approx. jaccard distances along with detailed performance analytics.')
    parser.add_argument("--filename",
             default='datafile.csv', help='filepath to file containing the lines to be sampled.')
    parser.add_argument("--num_signatures", type=int,
             default=50, help='(10,..] number of hash signatures to use.')
    parser.add_argument("--num_samples", type=int,
             default=100, help='number of input records to read from datafile.')
    parser.add_argument("--verify", type=int,
             default=0, help='whether or not to compute true jaccard distances for verification purposes')
    parser.add_argument("--largediff", type=float,
             default=0.3, help='small float specifying what is large error in differene between |approx-true| jaccard distances')
    parser.add_argument("--visualize_patterns", type=int,
             default=0, help='[0/1] do quantized display to help visualize patterns in the distances')
    parser.add_argument("--vizthreshold", type=int,
             default=4, help='[0..9] integer threshold above which quantized jaccard distances are displayed for visualiation purposes')
    parser.add_argument("--lsh_nbins", type=int,
             default=10, help='number of bins x to which to hash-compress the resulting mapping of tuples of num_signatures/x column cell values')
    parser.add_argument("--debug", type=int,
             default=3, help='debug level of output details [ERROR=1, WARNING=2, INFO=3, DEBUG=4]')
    parser.add_argument("--op", type=str,
             default="lshash", help='number of bins x to which to hash-compress the resulting mapping of tuples of num_signatures/x column cell values')
    args = parser.parse_args()
    parser.print_help()

    print >>sys.stderr, '-' * 80
    print >>sys.stderr, "file to scan:", args.filename
    print >>sys.stderr, "lines to read:", args.num_samples
    print >>sys.stderr, "num. signatures:", args.num_signatures
    print >>sys.stderr, "vizthreshold", args.vizthreshold
    print >>sys.stderr, "cross-verify:", args.verify
    print >>sys.stderr, "error threshold", args.largediff
    print >>sys.stderr, '-' * 80
