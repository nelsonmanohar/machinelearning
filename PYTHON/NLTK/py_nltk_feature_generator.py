#!/usr/bin/env python
# -*- coding: latin-1 -*-


# #########################################################################
import warnings
warnings.simplefilter('default')
# -------------------------------------------------------------------------
import time
import nltk
import string
import random
import operator as op
import os.path
# -------------------------------------------------------------------------
import enchant                              # http://stackoverflow.com/questions/3788870/how-to-check-if-a-word...
from nltk.corpus import words               # http://stackoverflow.com/questions/3788870/how-to-check-if-a-word...
from nltk.corpus import brown
from nltk.corpus import stopwords           # http://stackoverflow.com/questions/22763224
from nltk.corpus import wordnet
from nltk.chunk import named_entity
from nltk.tokenize import sent_tokenize
from nltk.stem.snowball import SnowballStemmer
# -------------------------------------------------------------------------
from collections import defaultdict
from collections import Counter
# -------------------------------------------------------------------------
from py_nltk_html_tag_cleaner import *
# #########################################################################


# #########################################################################
CACHE = {}
# #########################################################################


# #########################################################################
def binarize(my_function, decision_threshold=0):
    global CACHE

    def wrapper(*args, **keywords):
        key = [my_function, ]
        key.extend(args)
        key = tuple(key)
        if key in CACHE:
            return CACHE[key]
        ret = my_function(*args, **keywords)
        if isinstance(ret, type(True)):
            CACHE[key] = int(ret)
            return CACHE[key]
        if ret > decision_threshold:
            CACHE[key] = 1
            return CACHE[key]
        CACHE[key] = 0
        return CACHE[key]
    return wrapper
# #########################################################################


# #########################################################################
def stratify(my_function, decision_threshold=0):
    global CACHE

    def wrapper(*args, **keywords):
        key = [my_function, ]
        key.extend(args)
        key = tuple(key)
        if key in CACHE:
            return CACHE[key]
        endpoints = keywords['endpoints']
        print endpoints
        if labels in keywords:
            labels = keywords['labels']
        else:
            labels = [str(x) for x in endpoints]
        print labels
        ret = my_function(args)
        print ret
        idx = [i for (i, x) in enumerate(endpoints) if ret >= x]
        if not idx:
            idx = "NA"
        else:
            idx = labels[idx]
        print idx
        CACHE[key] = idx
        return CACHE[key]
    return wrapper
# #########################################################################

# #########################################################################
class feature_generator(object):
    DICTIONARY = enchant.Dict("en_US")
    STOPWORDS = set(stopwords.words('english'))
    DIGITS = set(string.digits)
    LETTERS = set(string.letters)
    PUNCTUATION = set(string.punctuation)
    # --------------------------------------------------------------------- 
    NOUNPHRASE_GRAMMAR = "NP: {<DT>?<JJ>*<NN>}"
    NOUNPHRASE = nltk.RegexpParser(NOUNPHRASE_GRAMMAR)
    # --------------------------------------------------------------------- 
    SYNSETS = {}
    HYPONYMS = {}
    ANTONYMS = {}
    HYPERNYMS = {}

    # #####################################################################
    def condfreq(self, tags, debug=0):
        d = {}
        for (word, pos) in tags:
            if word not in d:
                d[word] = defaultdict(int)
            d[word][pos] = d[word][pos] + 1
        n = {}
        for word in d:
            q = d[word]
            n[word] = sorted(q.items(), key=op.itemgetter(1), reverse=True)[0]
            if debug:
                print repr(word), q, n[word]
        return n
    # #####################################################################

    # #####################################################################
    def __init__(self, filename="", words=None, text=None, top_n=250, minwordlen=2, minrepeats=4, encoding="utf-8", extra_featureset=True, do_chunking=False, debug=0):
        t0 = time.time()
        # -----------------------------------------------------------------
        def timeit(what, t0):
            t = time.time()
            print "%s %8.5f seconds" % (what, t - t0)
            return t
        # -----------------------------------------------------------------

        self.extra_featureset = extra_featureset
        self.debug = debug
        self.HTML = text
        # -----------------------------------------------------------------

        self.separator = "\t|\t"
        self.filename = filename
        self.WORDS = words
        self.TEXT = text
        # -----------------------------------------------------------------

        if filename:
            with open(self.filename, 'r') as fp:
                self.HTML = fp.read()
        # -----------------------------------------------------------------

        if filename or text:
            self.HTML = clean_javascript(self.HTML)
            t0 = timeit("JAVASCPT", t0)
            self.TEXT = strip_tags(self.HTML)
            t0 = timeit("HTML/CSS", t0)
            self.WORDS = nltk.word_tokenize(" ".join([word.decode(encoding) for word in self.TEXT.split() if len(word)<30]))
            self.TEXT = " ".join([word.decode(encoding) for word in self.TEXT.split()])
            self.WORDS = nltk.Text(self.WORDS)
            self.N = len(self.WORDS)
            t0 = timeit("NLTK/TXT", t0)
        # -----------------------------------------------------------------

        if words:
            self.TEXT = " ".join(words)
            self.WORDS = nltk.Text(words)
            self.N = len(self.WORDS)
        # -----------------------------------------------------------------

        if not self.filename and not words and not text:
            K = 500000
            self.WORDS = brown.words()[:K]
            self.GOLDEN_TAGS = brown.tagged_words()[:K]
            self.N = len(self.WORDS)
        else:
            golden_tags_filename = '%s.goldentags' % filename
            self.GOLDEN_TAGS = []
            if os.path.isfile(golden_tags_filename):
                with open(golden_tags_filename) as fp:
                    lines = [line.strip() for line in fp.readlines() if line.strip()]
                    for line in lines:
                        items = line.split(self.separator)
                        self.GOLDEN_TAGS.append(items)
            if not len(self.GOLDEN_TAGS):
                if do_chunking:     # until semantic partitioning done (div or paragraphs) asshole who thinks knows better
                    start = 0
                    m = 10
                    for stop in range(self.N/m, self.N, self.N/m):
                        print "NTLK/GLD", start, stop
                        self.GOLDEN_TAGS = self.GOLDEN_TAGS + nltk.pos_tag(self.WORDS[start:stop])
                        start = stop
                else:
                    self.GOLDEN_TAGS = nltk.pos_tag(self.WORDS)
                with open(golden_tags_filename, 'w') as fp:
                    for golden_tag in self.GOLDEN_TAGS:
                        fp.write("%s%s%s\n" % (golden_tag[0].encode('utf-8'), self.separator, golden_tag[1]))
        t0 = timeit("NLTK/GLD", t0)
        # -----------------------------------------------------------------

        self.BROWN_TAGS = nltk.ConditionalFreqDist(nltk.corpus.brown.tagged_words())
        t0 = timeit("BRWN/TAG", t0)
        # -----------------------------------------------------------------

        self.train_ngram_tagger(split_ratio=5)
        self.TAGS = self.predict_ngram_tagger()
        t0 = timeit("NGRM/POS", t0)
        # -----------------------------------------------------------------

        self.CDF_TAGS = nltk.ConditionalFreqDist(self.TAGS)
        t0 = timeit("CDF_TAGS", t0)
        # -----------------------------------------------------------------

        self.TOPWORDS = set([x[0] for x in Counter([word for word in self.WORDS if len(word) > minwordlen]).most_common(top_n) if x[1] > minrepeats])
        t0 = timeit("TOPWORDS", t0)
        # -----------------------------------------------------------------

        self.TOPNAMES = set([x[0] for x in Counter((word for (word, tag) in self.TAGS if len(word) >= minwordlen and tag.startswith('NN'))).most_common(top_n) if x[1] > minrepeats])
        t0 = timeit("TOPNAMES", t0)
        # -----------------------------------------------------------------

        if self.extra_featureset:
            self.TOPADJJS = set([x[0] for x in Counter((word for (word, tag) in self.TAGS if len(word) >= minwordlen and tag.startswith('JJ'))).most_common(top_n) if x[1] > minrepeats])
            t0 = timeit("TOPADJJS", t0)
            self.TOPVERBS = set([x[0] for x in Counter((word for (word, tag) in self.TAGS if len(word) >= minwordlen and tag.startswith('VB'))).most_common(top_n) if x[1] > minrepeats])
            t0 = timeit("TOPVERBS", t0)
            suffix_fdist = nltk.FreqDist()
            for word in brown.words():
                if len(word) < minwordlen+1: next
                word = word.lower()
                suffix_fdist[word[-1:]] += 1
                suffix_fdist[word[-2:]] += 1
                suffix_fdist[word[-3:]] += 1
            self.SUFFIXES = [suffix for (suffix, count) in suffix_fdist.most_common(10)]
            t0 = timeit("SUFFIXES", t0)
        # -----------------------------------------------------------------

        print len(self.WORDS)
        return
    # #####################################################################

    # #####################################################################
    def train_ngram_tagger(self, split_ratio=20, top_n=10000, double_bootstrap=False):
        brown_sents = nltk.corpus.brown.tagged_sents(categories=['news'])
        words = nltk.corpus.brown.words(categories='news')
        fd = nltk.FreqDist(words)
        cfd = self.BROWN_TAGS
        most_freq_words = fd.most_common(top_n)
        likely_tags = dict((word, cfd[word].max()) for (word, _) in most_freq_words)
        random_idx = self.randomize_idx(range(len(brown_sents)), K=int(len(brown_sents)/split_ratio) + 1)
        train_sents = [s for (i, s) in enumerate(brown_sents) if i in set(random_idx)]
        t1 = nltk.UnigramTagger(model=likely_tags, backoff=nltk.DefaultTagger('NN'))
        print "NLTK/TGR EVAL(BOOT) T1:", t1.evaluate(brown_sents)
        t2 = nltk.BigramTagger(train_sents,  backoff=t1)
        print "NLTK/TGR EVAL(BOOT) T2:", t2.evaluate(brown_sents)
        t3 = nltk.TrigramTagger(train_sents, backoff=t2)
        print "NLTK/TGR EVAL(BOOT) T3:", t3.evaluate(brown_sents)
        # -----------------------------------------------------------------

        if (double_bootstrap):
            # get bootstrap golden tags/ground truth from above t3
            self.SENTENCES = nltk.pos_tag_sents(sent_tokenize(self.TEXT))
            # -------------------------------------------------------------

            # select random (target sentences and brown sentences) to re-train with
            brown_sents = self.SENTENCES + brown_sents
            random_idx = self.randomize_idx(range(len(brown_sents)), K=int(len(brown_sents)/split_ratio) + 1)
            train_sents = [s for (i, s) in enumerate(brown_sents) if i in set(random_idx)]
            # -------------------------------------------------------------

            # retrain with the new random (target sentences and brown sentences)
            t2 = nltk.BigramTagger(train_sents,  backoff=t1)
            print "NLTK/TGR EVAL(BOOT) T2:", t2.evaluate(brown_sents)
            t3 = nltk.TrigramTagger(train_sents, backoff=t2)
            print "NLTK/TGR EVAL(BOOT) T3:", t3.evaluate(brown_sents)
            # -------------------------------------------------------------
            print "NLTK/TGR EVAL(TEXT):", self.t3.evaluate(self.SENTENCES)

        self.NGRAM_POS_TAGGER = t3

        return self.NGRAM_POS_TAGGER
    # #####################################################################

    # #####################################################################
    def randomize_idx(self, idx=(), K=None):
        random.shuffle(idx)
        idx = idx[:K]
        return idx
    # #####################################################################

    # #####################################################################
    def predict_ngram_tagger(self):
        self.TAGS = self.NGRAM_POS_TAGGER.tag(self.WORDS)
        return self.TAGS
    # #####################################################################

    # #####################################################################
    @binarize
    def is_stopword(self, word):
        return word in self.STOPWORDS
    # #####################################################################

    # #####################################################################
    @binarize
    def is_capitalized(self, word):
        return word.istitle()
    # #####################################################################

    # #####################################################################
    @binarize
    def is_itemization(self, idx, word):  # approximate feature
        tag = self.get_pos_tag_for_word_at(idx, ttype="BOOTSTRAP")
        prev_word = self.get_word_at(idx-1)
        next_word = self.get_word_at(idx+1)
        return prev_word == ',' and tag.startswith('N') and next_word == ','
    # #####################################################################

    # #####################################################################
    @binarize
    def is_end_of_sentence(self, idx, word):  # approximate feature
        prev_word = self.get_word_at(idx-1)
        next_word = self.get_word_at(idx+1)
        return self.is_english(prev_word) and word == '.' and self.is_english(next_word) and self.is_capitalized(next_word)
    # #####################################################################

    # #####################################################################
    @binarize
    def is_english(self, word, tolower=False, redundant=False):
        if self.synsets:
            return True
        if any([x.isdigit() for x in word]) and not all([x.isdigit() or x in string.punctuation for x in word]):
            return False
        if not redundant:
            return False
        if tolower:
            word = word.lower()
        if self.DICTIONARY.check(word):
            return True
        if word in words.words():
            return True
        return False
    # #####################################################################

    # #####################################################################
    def is_nounphrase(self, idx, word):
        try:
            sentence = [self.TAGS[idx-1], self.TAGS[idx+0], self.TAGS[idx+1]]
            result = self.NOUNPHRASE.parse(sentence)
            if len(result):
                return 1
        except:
            return 0
        return 0
    # #####################################################################

    # #####################################################################
    @binarize
    def has_punctuation(self, word):
        return any([True if x in self.PUNCTUATION else False for x in word])
    # #####################################################################

    # #####################################################################
    @binarize
    def not_alphanumeric(self, word):
        return all([x.isdigit() or x.isalpha() for x in word])
    # #####################################################################

    # #####################################################################
    @binarize
    def is_punctuation(self, word):
        return all([True if x in self.PUNCTUATION else False for x in word])
    # #####################################################################

    # #####################################################################
    @binarize
    def has_numbers(self, word):
        return any([True if x in self.DIGITS else False for x in word])
    # #####################################################################

    # #####################################################################
    @binarize
    def is_topword(self, word):
        return word in self.TOPWORDS
    # #####################################################################

    # #####################################################################
    @binarize
    def is_topadjective(self, word):
        return word in self.TOPADJJS
    # #####################################################################

    # #####################################################################
    @binarize
    def is_topverb(self, word):
        return word in self.TOPVERBS
    # #####################################################################

    # #####################################################################
    @binarize
    def is_topname(self, word):
        return word in self.TOPNAMES
    # #####################################################################

    # #####################################################################
    @binarize
    def has_all_caps(self, word):
        return word.isupper()
    # #####################################################################

    # #####################################################################
    def my_word_shape_generator(self, word, separators="-_.: ", m=3):
        if len(word) < m:
            return "NA"
        if not any([x in self.LETTERS for x in word]):
            return "ddd"
        p = [word.find(c) for c in separators if c in word[1:-1]]
        if len(p):
            p = min(p)+1
            first_part = word[:p]
            last_part = word[p:]
            if first_part[0] == first_part[0].upper() and last_part[0] == last_part[0].upper():
                return 'X-X'
            if first_part[0] == first_part[0].upper() and not last_part[0] == last_part[0].upper():
                return 'X-x'
            if not first_part[0] == first_part[0].upper() and not last_part[0] == last_part[0].upper():
                return 'x-x'
            return 'x-X'
        if all([True if x.upper() == x else False for x in word]):
            return 'XXX'
        if self.is_capitalized(word):
            return 'Xxx'
        return 'xxx'
    # #####################################################################

    # #####################################################################
    def get_pos_tag_for_word_at(self, idx, ttype="BOOTSTRAP"):
        try:
            if "GOLDEN" in ttype:
                return self.GOLDEN_TAGS[idx][1]
            else:
                return self.TAGS[idx][1]
        except:
            return "NA"
    # #####################################################################

    # #####################################################################
    def get_word_at(self, idx):
        if idx < self.N:
            return self.WORDS[idx]
        return ""
    # #####################################################################

    # #####################################################################
    def get_wordlen_for_word_at(self, idx):
        if idx < self.N:
            return len(self.WORDS[idx])
        return 0
    # #####################################################################

    # #####################################################################
    @binarize
    def has_whitespace(self, word):
        return any([x in string.whitespace for x in word])
    # #####################################################################

    # #####################################################################
    @binarize
    def has_ending(self, word, ending, tolower=False):
        if tolower:
            word = word.lower()
        if len(word) > 4:
            return word.endswith(ending)
        return False
    # #####################################################################

    # #####################################################################
    def get_stem(self, word):
        if self.is_english(word):
            stemmed_word = SnowballStemmer("english").stem(word)
            stem = word[len(stemmed_word):]
        else:
            stem = "NA"
        return stem
    # #####################################################################

    # #####################################################################
    def sfx_features(self, word, tolower=False):
        feats = {}
        if tolower:
            word = word.lower()
        for suffix in common_suffixes:
            feats['endswith({})'.format(suffix)] = word.endswith(suffix)
        return feats
    # #####################################################################

    # #####################################################################
    def simplified_pos(self, tag):
        return named_entity.simplify_pos(tag)
    # #####################################################################

    # #####################################################################
    def shape(self, word):
        return named_entity.shape(word)
    # #####################################################################

    # #####################################################################
    def get_synsets(self, word):
        if word in self.SYNSETS:
            self.synsets = wordnet.synsets(word)
            return self.synsets
        self.synsets = wordnet.synsets(word)
        self.SYNSETS[word] = self.synsets[:]
        return self.synsets
    # #####################################################################

    # #####################################################################
    def get_antonyms(self, word, synsets=None):
        if word in self.ANTONYMS:
            return self.ANTONYMS[word]
        antonyms = self.get_wordnet_field(word, synsets=synsets, field="antonyms")
        self.ANTONYMS[word] = antonyms
        return antonyms
    # #####################################################################

    # #####################################################################
    def get_hypernyms(self, word, synsets=None):
        if word in self.HYPERNYMS:
            return self.HYPERNYMS[word]
        hypernyms = self.get_wordnet_field(word, synsets=synsets, field="hypernyms")
        self.HYPERNYMS[word] = hypernyms
        return hypernyms
    # #####################################################################

    # #####################################################################
    def get_hyponyms(self, word, synsets=None):
        if word in self.HYPONYMS:
            return self.HYPONYMS[word]
        hyponyms = self.get_wordnet_field(word, synsets=synsets, field="hyponyms")
        self.HYPONYMS[word] = hyponyms
        return hyponyms
    # #####################################################################

    # #####################################################################
    def get_wordnet_field(self, word, synsets=None, field="antonyms"):
        if not synsets:
            synsets = self.get_synsets(word)
        if field in "synonyms":     # like
            return self.synsets
        if not synsets:
            return []
        # -----------------------------------------------------------------
        first_lemma = synsets[0]
        if field in "antonyms":     # opposite-of
            return first_lemma.lemmas()[0].antonyms()
        if field in "hypernyms":    # part-of
            return first_lemma.hypernyms()
        if field in "hyponyms":     # is-a
            return first_lemma.hyponyms()
        if field in "variability":  # branching options
            return [len(synset.lemma_names()) for synset in synsets]
        return []
    # #####################################################################

    # #####################################################################
    def top_freqs(self, n=100, m=3, use_counter=True):
        self.FREQS = defaultdict(int)
        if not use_counter:
            for w in self.WORDS:
                if len(w) > m:
                    self.FREQS[w] = self.FREQS[w] + 1
            topmost = sorted(FREQS.items(), key=op.itemgetter(1), reverse=True)[0:n]
        else:
            topmost = Counter([word for word in self.WORDS if len(word) > m]).most_common(n)
        return topmost
    # #####################################################################

    # #####################################################################
    def most_common_pos_tag(self, word):
        if word in self.CDF_TAGS:
            if self.debug > 1:
                print self.CDF_TAGS[word]
                print self.CDF_TAGS[word].most_common()
                print self.CDF_TAGS[word].most_common()[0]
                print self.CDF_TAGS[word].most_common()[0][0]
            ret = self.CDF_TAGS[word].most_common()[0][0]
            return ret
        return "NA"
    # #####################################################################

    # #####################################################################
    def most_common_brown_pos_tag(self, word):
        if word in self.BROWN_TAGS:
            if self.debug > 1:
                print self.BROWN_TAGS[word]
                print self.BROWN_TAGS[word].most_common()
                print self.BROWN_TAGS[word].most_common()[0]
                print self.BROWN_TAGS[word].most_common()[0][0]
            ret = self.BROWN_TAGS[word].most_common()[0][0]
            return ret
        return "NA"
    # #####################################################################

    # #####################################################################
    def print_features(self, fvec):
        if self.debug > 1:
            print '-' * 80
            print repr(word), idx, tag
        if self.debug:
            print fvec
        return
    # #####################################################################

    # #####################################################################
    def generate_features(self, word, idx, debug=1):
        # -----------------------------------------------------------------
        self.word = word
        self.synsets = self.get_synsets(word)
        self.idx = idx
        tag = self.get_pos_tag_for_word_at(idx, ttype="BOOTSTRAP")
        self.tag = tag
        # -----------------------------------------------------------------
        fvec = {}
        if debug:
            fvec['word'] = self.get_word_at(idx).encode('utf-8')
        # -----------------------------------------------------------------
        fvec['is_english'] = self.is_english(word)
        fvec['is_capitalized'] = self.is_capitalized(word)
        fvec['has_punctuation'] = self.has_punctuation(word)
        fvec['is_punctuation'] = self.is_punctuation(word)
        fvec['not_alphanumeric'] = self.not_alphanumeric(word)
        fvec['has_numbers'] = self.has_numbers(word)
        fvec['all_caps'] = self.has_all_caps(word)
        # -----------------------------------------------------------------
        fvec['m1_wordlen'] = self.get_wordlen_for_word_at(idx-1)
        fvec['i0_wordlen'] = self.get_wordlen_for_word_at(idx+0)
        fvec['p1_wordlen'] = self.get_wordlen_for_word_at(idx+1)
        # -----------------------------------------------------------------
        fvec['m3_postag'] = self.get_pos_tag_for_word_at(idx-3, ttype="BOOTSTRAP")
        fvec['m2_postag'] = self.get_pos_tag_for_word_at(idx-2, ttype="BOOTSTRAP")
        fvec['m1_postag'] = self.get_pos_tag_for_word_at(idx-1, ttype="BOOTSTRAP")
        fvec['i0_postag'] = self.get_pos_tag_for_word_at(idx+0, ttype="BOOTSTRAP")
        fvec['p1_postag'] = self.get_pos_tag_for_word_at(idx+1, ttype="BOOTSTRAP")
        fvec['p2_postag'] = self.get_pos_tag_for_word_at(idx+2, ttype="BOOTSTRAP")
        fvec['p3_postag'] = self.get_pos_tag_for_word_at(idx+3, ttype="BOOTSTRAP")
        fvec['nltk_pos']  = self.get_pos_tag_for_word_at(idx+0, ttype="GOLDEN")
        # -----------------------------------------------------------------
        fvec['simplified_pos'] = tag[0]
        fvec['basic_shape'] = self.shape(word)
        fvec['wordshape'] = self.my_word_shape_generator(word)
        # -----------------------------------------------------------------
        fvec['dominant_pos'] = self.most_common_pos_tag(word)
        fvec['dominant_brown_pos'] = self.most_common_brown_pos_tag(word)
        fvec['high_freq'] = self.is_topword(word)
        fvec['is_topname'] = self.is_topname(word)
        # -----------------------------------------------------------------
        if self.extra_featureset:
            fvec['is_topadj'] = self.is_topadjective(word)
            fvec['is_topverb'] = self.is_topverb(word)
        # -----------------------------------------------------------------
        fvec['num_synonyms'] = len(self.get_wordnet_field(word, synsets=self.synsets, field="synonyms"))
        fvec['num_antonyms'] = len(self.get_wordnet_field(word, synsets=self.synsets, field="antonyms"))
        fvec['num_hyponyms'] = len(self.get_wordnet_field(word, synsets=self.synsets, field="hyponyms"))
        fvec['alternatives'] = sum(self.get_wordnet_field(word, synsets=self.synsets, field="variability"))
        # -----------------------------------------------------------------
        fvec['ends_with_ed'] = self.has_ending(word, "ed")
        fvec['ends_with_nt'] = self.has_ending(word, "nt")
        fvec['ends_with_es'] = self.has_ending(word, "es")
        fvec['ends_with_ng'] = self.has_ending(word, "ng")
        fvec['ends_with_ly'] = self.has_ending(word, "ly")
        fvec['ends_with_on'] = self.has_ending(word, "on")
        fvec['word_stem'] = self.get_stem(word)
        # -----------------------------------------------------------------
        if self.extra_featureset:
            fvec['itemization'] = self.is_itemization(idx, word)
            fvec['end_sentence'] = self.is_end_of_sentence(idx,  word)
            fvec['is_nounphrase'] = self.is_nounphrase(idx, word)
        self.print_features(fvec)
        return fvec
    # #####################################################################

    # #####################################################################
    def generate_vectors(self):
        fvecs = [self.generate_features(word, idx) for (idx, word) in enumerate(self.WORDS)]
        return fvecs
    # #####################################################################


# #########################################################################


# #########################################################################
if __name__ == "__main__":
    # -----------------------------------------------------------------
    FEATURE_GENERATOR = feature_generator(filename='index.html', top_n=100, minwordlen=2, encoding="utf-8")
    # -----------------------------------------------------------------
    fvecs = FEATURE_GENERATOR.generate_vectors()
    # -----------------------------------------------------------------
    for i, fv in enumerate(fvecs):
         print i, fv
    # -----------------------------------------------------------------
